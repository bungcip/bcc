Nom (version 2.0) FAQ
==================================

This document contain my experience using nom. Basically nom official document is sparse.
I hope with this, people can find this manual & how to useful.



HOW To
======
- I want to return parser result other than slice in named! macro.
Use this syntax to return string

```rust
named!(pub sys_path, delimited!(char!('<'), is_not!(">"), char!('>')));
```




ERROR EXPLANATION
=================

-  expected enum `bla bla bla`, found enum `std::result::Result`
```
 named!(pub rel_or_sys_path<IncludePath>, alt_complete!(
    |  _____^ starting here...
105 | |         map_res!(sys_path, IncludePath::System) |
106 | |         map_res!(rel_path, IncludePath::Relative)
107 | |     ));
    | |_______^ ...ending here: expected enum `lexer::IncludePath`, found enum `std::result::Result`
    |
    = note: expected type `lexer::IncludePath<'_>`
    = note:    found type `std::result::Result<_, _>`
    = note: this error originates in a macro outside of the current crate
```

This is because map_res!() returning a Result type. use map!() here to fix the problem

-

```
error: no rules expected the token `i`
  --> <do_parse macros>:69:23
   |
69 | { do_parse ! ( __impl $ i , 0usize , $ ( $ rest ) * ) } ) ;
   |                       ^^^

error: Could not compile `bcpp`.
```


Macro Definition
=================


- delimited!(begin_parser, value_parser, end_parser)

create parser to get value between surrounding delimiter.
Use this if you need the value content.
Example:

Get Value between < >
```rust
named!(pub sys_path, delimited!(char!('<'), is_not!(">"), char!('>')));
```

- map_res!(parser)

use this function to map the result value of parser. this is useful
if you want convert the value produced by parser to something else.

Convert the parser result (&[u8]) to string slice
```rust
named!(pub character<&str>, map_res!(alphanumeric, str::from_utf8));
```

- map!(parser)
use this function to map the result to other function



