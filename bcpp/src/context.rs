use std::path::Path;
use std::fs::File;
use std::io::BufReader;
use std::io::Read;

use std::collections::HashMap;

use token::Token;
use parser;
use parser::Parser;
use ast::*;
use expander::*;

// Create the Error, ErrorKind, ResultExt, and Result types
error_chain! { 
    links {
        Parser(parser::Error, parser::ErrorKind);
    }

    foreign_links {
        Io(::std::io::Error);
    }
}

pub struct Context<'a> {
    path: &'a str,
    main_source: Vec<u8>,
    expand_table: HashMap<&'a str, ExpandValue<'a>>,
    tokens: Vec<Token<'a>>,
}

impl<'a> Context<'a> {
    fn open(path: &'a str) -> Result<Context<'a>> {
        let file = File::open(path)?;
        let mut reader = BufReader::new(file);
        let mut buf : Vec<u8> = Vec::new();
        let size = reader.read_to_end(&mut buf)?;

        Ok(Context {
            path: path,
            main_source: buf,
            expand_table: HashMap::new(),
            tokens: vec![],
        })
    }

    // parse the current file and expand macro
    fn process(&'a mut self) -> Result<()> {
        // parse
        let nodes = {
            let mut parser = Parser::with_bytes(&self.main_source);
            parser.parse()?
        };

        for node in nodes {
            let mut expand_later: Option<Vec<Token<'a>>> = None;

            match node {
                Node::Directive(Directive::Define(def)) => {
                    match def {
                        Define::Constant(name, value) => {
                            self.expand_table.insert(name, ExpandValue::TokenVec(value));
                        }
                    }

                },
                Node::TokenVec(tokens) => {
                    expand_later = Some(tokens);
                },
                _ => panic!("Belum siap")
            }

            // workaround for borrow checker
            if let Some(tokens) = expand_later {
                let tokens = expand(&self.expand_table, tokens);
                self.tokens.extend(tokens);
            }

        }

        Ok(())
    }

}


/// Expand give token slice and return specific token from
/// table
pub fn expand<'a>(table: &'a HashMap<&'a str, ExpandValue<'a>>, tokens: Vec<Token<'a>>) -> Vec<Token<'a>> {
    let mut result : Vec<Token<'a>> = vec![];

    for token in tokens {
        match token {
            Token::Ident(id) => {
                match table.get(id){
                    Some(expand_value) => match expand_value {
                        &ExpandValue::Constant(string_value) => {
                            let replacement = Token::Ident(string_value);
                            result.push(replacement);
                        },
                        &ExpandValue::TokenVec(ref token_vec) => {
                            let token_vec = token_vec.clone();
                            let expanded = expand(&table, token_vec);
                            result.extend(expanded);
                        },
                        // _ => unimplemented!()
                    },
                    None => {
                        result.push(token);
                    },
                }
            }
            other => {
                result.push(other);
            }
        };

    }

    result
}



#[cfg(test)]
mod testing {
    use super::*;

    #[test]
    fn test_simple(){
        let cxt = Context::open("tests/01_define.c");
        let mut cxt = cxt.unwrap();

        let result = cxt.process();
        result.unwrap();
    }
}