use lexer::Lexer;
use token::Token;
use ast::*;

// Create the Error, ErrorKind, ResultExt, and Result types
error_chain! { 
    types {
        Error, ErrorKind, ResultExt, Result;
    }
}

pub struct Parser<'a> {
    lexer: Lexer<'a>,
}

impl<'a> Parser<'a> {
    pub fn new(input: &'a str) -> Self {
        Parser {
            lexer: Lexer::new(input)
        }
    }

    pub fn with_bytes(input: &'a [u8]) -> Self {
        Parser {
            lexer: Lexer::with_bytes(input)
        }
    }

    //
    pub fn parse(&mut self) -> Result<Vec<Node<'a>>> {
        let mut nodes = vec![];
        loop {
            let token = self.lexer.next_token();
            let node = match token {
                Token::Hash => {
                    let directive = Self::parse_directive(&mut self.lexer)?;
                    Node::Directive(directive)
                },
                first @ Token::Ident(_) => {
                    let mut tokens : Vec<Token<'a>> = Vec::with_capacity(32);
                    Self::parse_logical_line(&mut self.lexer, &mut tokens, first)?;
                    Node::TokenVec(tokens)
                },
                Token::EndOfFile => break,
                _ =>             panic!("sampai sini"),
            };

            nodes.push(node)
        }

        Ok(nodes)
    }

    // parse directive
    pub fn parse_directive(lexer: &mut Lexer<'a>) -> Result<Directive<'a> >{
        let directive = lexer.next_token();

        let result = match directive {
            Token::Ident("define") => Directive::Define(Self::parse_define(lexer)?),
            Token::Ident("undef") => Directive::Undef(Self::parse_undef(lexer)?),
            Token::Ident("ifdef") => Directive::IfDef(Self::parse_ifdef(lexer)?),
            _ => panic!("sampai situ"),
        };

        Ok(result)
    }

    pub fn parse_define(lexer: &mut Lexer<'a>) -> Result<Define<'a>> {
        let name = Self::parse_ident(lexer)?;
        let mut tokens : Vec<Token<'a>> = Vec::with_capacity(2);

        loop {
            let token = lexer.next_token();
            match token {
                Token::EndOfLine | Token::EndOfFile => break,
                _ => tokens.push(token),
            }
        }
        Ok(Define::Constant(name, tokens))
    }

    pub fn parse_undef(lexer: &mut Lexer<'a>) -> Result<Undef<'a>> {
        let name = Self::parse_ident(lexer)?;
        Ok(Undef(name))
    }

    pub fn parse_ifdef(lexer: &mut Lexer<'a>) -> Result<IfDef<'a>> {
        let name = Self::parse_ident(lexer)?;
        Ok(IfDef(name))
    }

    pub fn parse_ident(lexer: &mut Lexer<'a>) -> Result<&'a str> {
        let token = lexer.next_token();
        match token {
            Token::Ident(ident) => {
                Ok(ident)
            }
            _ => {
                let msg = format!("Expected Identifier but got '{}' instead", token);
                Err(msg.into())
            }
        }
    }

    pub fn parse_logical_line(lexer: &mut Lexer<'a>, tokens: &mut Vec<Token<'a>>, first: Token<'a>) -> Result<()> {
        tokens.push(first);
        loop {
            match lexer.next_token() {
                Token::EndOfLine | Token::EndOfFile => return Ok(()),
                token => tokens.push(token),
            }
        }
    }

}

#[cfg(test)]
mod testing {
    use super::*;

    #[test]
    fn test_define(){
        let input = "#define FOO BAR";
        let mut parser = Parser::new(input);
        let nodes = parser.parse().unwrap();
        assert_eq!(nodes.len(), 1);
        assert_eq!(nodes[0], Node::Directive(Directive::Define(Define::Constant("FOO", vec![
            Token::Ident("BAR")
        ]))));
    }

    #[test]
    fn test_define_multi_word(){
        let input = "#define FOO BAR BAZ";
        let mut parser = Parser::new(input);
        let nodes = parser.parse().unwrap();
        assert_eq!(nodes.len(), 1);
        assert_eq!(nodes[0], Node::Directive(Directive::Define(Define::Constant("FOO", vec![
            Token::Ident("BAR"),
            Token::Ident("BAZ"),
        ]))));
    }

    #[test]
    fn test_undef(){
        let input = "#undef FOO";
        let mut parser = Parser::new(input);
        let nodes = parser.parse().unwrap();
        assert_eq!(nodes.len(), 1);
        assert_eq!(nodes[0], Node::Directive(Directive::Undef(Undef("FOO"))));
    }

    #[test]
    fn test_token_vec(){
        let input = "FOO BAR";
        let mut parser = Parser::new(input);
        let nodes = parser.parse().unwrap();
        assert_eq!(nodes.len(), 1);
        assert_eq!(nodes[0], Node::TokenVec(vec![Token::Ident("FOO"), Token::Ident("BAR")]));
    }


}
