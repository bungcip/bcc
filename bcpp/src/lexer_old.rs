
use nom;
use nom::{IResult,digit};

// Parser definition

use std::str;
use std::str::FromStr;


#[derive(PartialEq, Eq, Debug)]
pub struct Define<'parser> {
    name: &'parser str,
    value: &'parser str
}

impl<'parser> Define<'parser> {
    pub fn new(name: &'parser str, value: &'parser str) -> Define<'parser> {
        Define{ name: name, value: value }
    }
}

#[derive(PartialEq, Eq, Debug)]
pub enum IncludePath<'parser> {
    System(&'parser str),
    Relative(&'parser str)
}

#[derive(PartialEq, Eq, Debug)]
pub enum Ast<'parser> {
    Define(Define<'parser>),
    Undef(&'parser str),
    Include(IncludePath<'parser>),
    LogicalLine(Vec<&'parser str>),
}

pub struct CompilationUnit<'parser> {
    tt: Vec<Ast<'parser>>,
}

mod parser {
    use super::*;
    use nom::*;

    // define    
    named!(pub directive_define <Ast>, ws!(do_parse!(
        tag!("#")                                           >>
        tag!("define")                                      >>
        name: map_res!(alpha, str::from_utf8)               >>
        value: map_res!(not_line_ending, str::from_utf8)    >>
        (Ast::Define(Define::new(name, value)))
    )));

    /// undef
    named!(pub directive_undef <Ast>, ws!(do_parse!(
        tag!("#")                                           >>
        tag!("undef")                                       >>
        name: map_res!(alpha, str::from_utf8)               >>
        (Ast::Undef(name))
    )));

    /// file path
    named!(pub sys_path<&str>, map_res!(delimited!(char!('<'), is_not!(">"), char!('>')), str::from_utf8));
    named!(pub rel_path<&str>, map_res!(delimited!(char!('"'), is_not!("\""), char!('"')), str::from_utf8));
    named!(pub rel_or_sys_path<IncludePath>, alt_complete!(
        map!(sys_path, IncludePath::System) | 
        map!(rel_path, IncludePath::Relative)
    ));

    /// include directive
    named!(pub directive_include <Ast>, ws!(do_parse!(
        tag!("#") >>
        tag!("include") >>
        value: rel_or_sys_path >>
        ( Ast::Include(value) )
    )));

    /// logical line, sementara belum bisa ambil escape line \
    named!(pub logical_line<Ast>, do_parse!(
        content: map_res!(not_line_ending, str::from_utf8) >>
        (Ast::LogicalLine(vec![content]))
    ));

    named!(pub line<Ast>, alt_complete!(
        directive_define |
        directive_undef  |
        directive_include |
        logical_line
    ));

    named!(pub buffer<Vec<Ast> >, do_parse!(
        res: dbg_dmp!(many0!(line)) >>
        eof!()            >>
        (res)
    ));


    /// parse byte slice
    pub fn slice(content: &[u8]) -> CompilationUnit {
        let mut tt : Vec<Ast> = Vec::with_capacity(512);
        let mut buf = content;
        loop {
            let res = line(buf);
            match res {
                IResult::Done(rest, output) => {
                    tt.push(output);
                    buf = rest;
                    //panic!("ok {:?}", str::from_utf8(buf).unwrap())
                },
                _ => panic!("sementara error")
            };

            // eat EOL
            match eol(buf){
                IResult::Done(rest, _) => {
                    buf = rest
                },
                IResult::Incomplete(..) => panic!("nggak komplit"),
                IResult::Error(error) => {
                    panic!("{}", error);
                },
            }
        }

        CompilationUnit {
            tt: tt
        }
    }

}

#[cfg(test)]
mod test {
    use super::*;

    macro_rules! assert_done_eq {
        ($e:expr, $v:expr) => {{
            let res = $e;
            match res {
                ::nom::IResult::Done(_, value) => {
                    assert_eq!(value, $v);
                },
                ::nom::IResult::Error(error) => {
                    panic!("parser result is error. {:?}", error);
                },
                ::nom::IResult::Incomplete(needed) => {
                    panic!("parser result is incompleted. {:?}", needed);
                }
            }
        }}
    }


    #[test]
    fn test_define(){
        assert!(parser::directive_define(b"#define A Hello World").is_done());
        assert!(parser::directive_define(b"# define BB 10").is_done());
        assert!(parser::directive_define(b"   #define C 3 + 5").is_done());
        assert!(parser::directive_define(b"   #define Delta12 3 + 5").is_done());
    }

    #[test]
    fn test_undef(){
        assert_done_eq!(parser::directive_undef(b"#undef A"), Ast::Undef("A"));
        assert_done_eq!(parser::directive_undef(b"# undef     B"), Ast::Undef("B"));
        assert_done_eq!(parser::directive_undef(b"       # undef     C"), Ast::Undef("C"));
        assert_done_eq!(parser::directive_undef(b"# undef Delta       "), Ast::Undef("Delta"));
    }

    #[test]
    fn rel_or_sys_path(){
        assert_done_eq!(parser::rel_or_sys_path(b"<foo.h>"), IncludePath::System("foo.h"));
        assert_done_eq!(parser::rel_or_sys_path(b"\"foo.h\""), IncludePath::Relative("foo.h"));
    }

    #[test]
    fn test_include(){
        assert_done_eq!(
            parser::directive_include(b"#include <stdint.h>"), 
            Ast::Include(IncludePath::System("stdint.h"))
        );
    }

    #[test]
    fn test_logical_line(){
        assert_done_eq!(parser::logical_line(b"kucing garong"), Ast::LogicalLine(vec!["kucing garong"]));
        assert_done_eq!(parser::logical_line(b"kucing garong\n new line"), Ast::LogicalLine(vec!["kucing garong"]));
    }

    #[test]
    fn test_line(){
        assert_done_eq!(parser::line(b"#undef A"), Ast::Undef("A"));
        assert_done_eq!(parser::line(b"kucing garong"), Ast::LogicalLine(vec!["kucing garong"]));
        assert_done_eq!(parser::line(b"#include <stdint.h>"), Ast::Include(IncludePath::System("stdint.h")));
    }

    #[test]
    fn test_parse_slice(){
        let content = b"hello\nworld\r\n      #include <stdint.h>";
        let res = parser::slice(content);
        assert_eq!(res.tt.len(), 3);
        assert_eq!(res.tt[0], Ast::LogicalLine(vec!["hello"]));
        assert_eq!(res.tt[1], Ast::LogicalLine(vec!["world"]));
        assert_eq!(res.tt[2], Ast::Include(IncludePath::System("stdint.h")));
    }

    // #[test]
    // fn test_simple_buffer(){
    //     let content = b"hello\nworld";
    //     assert_done_eq!(parser::buffer(content), vec![Ast::LogicalLine(vec!["kucing garong"])]);
    // }

    #[test]
    fn test_buffer(){
        let content = br#"
            #include <stdint.h>
            #include <stdio.h>
            int main(){
                printf("hello world");
                return 0;
            }"#;

        println!("{}", str::from_utf8(content).unwrap());

        let cu = parser::slice(content);
        assert_eq!(cu.tt.len(), 6);
        assert_eq!(cu.tt[0], Ast::Include(IncludePath::System("stdint.h")));
        assert_eq!(cu.tt[1], Ast::Include(IncludePath::System("stdio.h")));
        assert_eq!(cu.tt[2], Ast::LogicalLine(vec!["        int main(){"]));
        assert_eq!(cu.tt[3], Ast::LogicalLine(vec![r#"            printf("hello world");"#]));
        assert_eq!(cu.tt[4], Ast::LogicalLine(vec!["            return 0;"]));
        assert_eq!(cu.tt[5], Ast::LogicalLine(vec!["        }"]));

    }
}

