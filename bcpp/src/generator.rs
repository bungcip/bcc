use std::io::prelude::*;
use std::io;
use std::fs::File;

use token::Token;
use ast::*;

/// generate output file based on ast node

pub struct Generator {
    ouput: File
}

pub fn generate<W: Write>(writer: &mut io::LineWriter<W>, nodes: &[Node]){
    for node in nodes {
        match *node {
            Node::Directive(_) => panic!("belum ada"),
            Node::TokenVec(ref vector) => {
                // NOTE: this is not ideal, but I cannot find how to join
                //       string separated with space in rust 
                //       idiomaticly
                let mut iterator = vector.iter();

                // first token is without formatting
                {
                    let first_token = iterator.next();
                    match first_token {
                        None => continue,
                        Some(token) => {
                            let s = format!("{}", token);
                            writer.write(s.as_bytes());
                        }
                    }
                }
                // other token have space
                for token in iterator {
                    let s = format!(" {}", token);
                    writer.write(s.as_bytes());
                }
                writer.write(b"\n");
            }
        }
    }
    writer.flush();
}



#[cfg(test)]
mod testing {
    use super::*;
    use std::io::Cursor;

    macro_rules! tokenizer {
        ($s:expr) => {{
            let mut lexer = ::lexer::Lexer::new($s);
            let mut tokens: Vec<::token::Token> = vec![];
            loop {
                match lexer.next_token(){
                    ::token::Token::EndOfFile => break,
                    token => tokens.push(token),
                }
            }
            tokens
        }}
    }

    macro_rules! token_vec {
        ($e:expr) => (Node::TokenVec($e))
    }

    macro_rules! assert_eq_output {
        ($e:expr, $o:expr) => {{
            let output = $o;
            let input = $e.get_ref().get_ref();
            let input = &input[0 .. output.len()];
            let input = ::std::str::from_utf8(input).unwrap();
            assert_eq!(input, output);
        }}
    }


    fn generate_ouput(s: &str) -> io::LineWriter<Cursor<Vec<u8>>> {
        // setting up a real File is much more slow than an in-memory buffer,
        // let's use a cursor instead
        let mut buf = Cursor::new(vec![0; 512]);
        let mut writer = io::LineWriter::new(buf);

        let token_vec = token_vec!(tokenizer!(s));
        let nodes = vec![token_vec];
        generate(&mut writer, nodes.as_slice());
        writer
    }

    #[test]
    fn test_simple(){
        let output = generate_ouput("kucing garong 1+2");
        assert_eq_output!(output, "kucing garong 1 + 2\n");
    }

   
}