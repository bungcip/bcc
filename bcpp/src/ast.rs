use token::Token;

pub struct CompilationUnit {

}

#[derive(Debug, Eq, PartialEq)]
pub enum Node<'a> {
    Directive(Directive<'a>),
    TokenVec(Vec<Token<'a>>)
}

#[derive(Debug, Eq, PartialEq)]
pub enum Directive<'a> {
    Define(Define<'a>),
    Undef(Undef<'a>),
    IfDef(IfDef<'a>)
}

#[derive(Debug, Eq, PartialEq)]
pub enum Define<'a> {
    Constant(&'a str, Vec<Token<'a>>)
}

#[derive(Debug, Eq, PartialEq)]
pub struct Undef<'a>(pub &'a str);

#[derive(Debug, Eq, PartialEq)]
pub struct IfDef<'a>(pub &'a str);

