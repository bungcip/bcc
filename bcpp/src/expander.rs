use std::path::Path;
use std::fs::File;
use std::io::BufReader;
use std::io::Read;

use std::collections::HashMap;

use token::Token;
use parser;
use parser::Parser;
use ast::*;

error_chain! { 
    links {
        // Parser(parser::Error, parser::ErrorKind);
    }

    foreign_links {
        Io(::std::io::Error);
    }
}


pub struct Expander<'a> {
    // table: HashMap<&'a str, Vec<Token<'a>>>
    table: HashMap<&'a str, ExpandValue<'a>>,
}

pub enum ExpandValue<'b> {
    Constant(&'b str),
    TokenVec(Vec<Token<'b>>)
}

impl<'a> Expander<'a> {
    pub fn new() -> Expander<'a> {
         let mut table: HashMap<&'a str, ExpandValue<'a>> = HashMap::new();
         Expander {
             table: table
         }
    }

    pub fn insert_constant(&mut self, name: &'a str, value: &'a str) {
        self.table.insert(name, ExpandValue::Constant(value));
    }


    pub fn insert_token_vec(&mut self, name: &'a str, value: Vec<Token<'a>>){
        self.table.insert(name, ExpandValue::TokenVec(value));
    }

    /// Expand give token slice and return specific token from
    /// table
    pub fn expand(&'a self, tokens: Vec<Token<'a>>) -> Vec<Token<'a>> {
        let mut result : Vec<Token<'a>> = vec![];

        for token in tokens {
            match token {
                Token::Ident(id) => {
                    match self.table.get(id){
                        Some(expand_value) => match expand_value {
                            &ExpandValue::Constant(string_value) => {
                                let replacement = Token::Ident(string_value);
                                result.push(replacement);
                            },
                            &ExpandValue::TokenVec(ref token_vec) => {
                                let token_vec = token_vec.clone();
                                let expanded = self.expand(token_vec);
                                result.extend(expanded);
                            },
                            // _ => unimplemented!()
                        },
                        None => {
                            result.push(token);
                        },
                    }
                }
                other => {
                    result.push(other);
                }
            };
            
        }

        result
    }

}



#[cfg(test)]
mod testing {
    use super::*;
    use lexer::Lexer;

    macro_rules! tokenizer {
        ($s:expr) => {{
            let mut lexer = Lexer::new($s);
            let mut tokens: Vec<Token> = vec![];
            loop {
                match lexer.next_token(){
                    Token::EndOfFile => break,
                    token => tokens.push(token),
                }
            }
            tokens
        }}
    }

    #[test]
    fn test_expand_constant_node(){
        let mut expander = Expander::new();
        expander.insert_constant("foo", "bar");
        
        let input = tokenizer!("baz foo baz");
        let expected = tokenizer!("baz bar baz");

        let output = expander.expand(input);
        assert_eq!(output, expected);
    }

    #[test]
    fn test_expand_token(){
        let mut expander = Expander::new();

        // output as is
        {
            expander.insert_token_vec("foo", vec![Token::Ident("A"), Token::Plus, Token::Ident("B")]);
            let input = tokenizer!("foo");
            let expected = tokenizer!("A + B");

            let output = expander.expand(input);
            assert_eq!(output, expected);
        }


        // add definition of A
        {
            expander.insert_token_vec("A", vec![Token::Ident("bar")]);
            let input = tokenizer!("foo");
            let expected = tokenizer!("bar + B");
            let output = expander.expand(input);
            assert_eq!(output, expected);
        }
        
        // add definition of B
        {
            expander.insert_token_vec("B", vec![Token::Ident("A")]);
            let input = tokenizer!("foo");
            let expected = tokenizer!("bar + bar");
            let output = expander.expand(input);
            assert_eq!(output, expected);
        }
        
        
    }
}