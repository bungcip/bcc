use std::fmt;
use std::fmt::Display;

#[derive(Debug, PartialEq, Copy, Clone, Eq)]
pub enum Token<'a> {
    Illegal,
    EndOfLine,
    EndOfFile,
    Other,

    // Literals are stored as strings
    Ident(&'a str),
    Integer(&'a str),

    // Operators
    Hash,
    Dollar,
    Question,
    Colon,
    Tilde,

    Ampersand,
    Pipe,

    Assign,
    Plus,
    Minus,
    Bang,
    Asterisk,
    Slash,
    LowerThan,
    GreaterThan,
    Equal,
    NotEqual,

    // Delimiters
    Comma,
    Semicolon,
    LeftParenthesis,
    RightParenthesis,
    LeftBrace,
    RightBrace,

    // Keywords
    Function,
    True,
    False,
    If,
    Else,
    Return,
}

impl<'a> Default for Token<'a> {
    // Choose an Illegal identifier as default
    // this should be overriden before being used
    fn default() -> Token<'a> {
        Token::Illegal
    }

}

impl<'a> Display for Token<'a> {
        // This trait requires `fmt` with this exact signature.
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            use self::Token::*;

            // Write strictly the first element into the supplied output
            // stream: `f`. Returns `fmt::Result` which indicates whether the
            // operation succeeded or failed. Note that `write!` uses syntax which
            // is very similar to `println!`.
            let value = match *self {
                Ident(value) => value,
                Integer(value) => value,
                Hash => "#",
                Dollar => "$",
                Question => "?",
                Colon => ":",
                Tilde => "~",

                Ampersand => "&",
                Pipe => "|",

                Assign => "=",
                Plus => "+",
                Minus => "-",
                Bang => "!",
                Asterisk => "*",
                Slash => "/",
                LowerThan => "<",
                GreaterThan => ">",
                Equal => "=",
                NotEqual => "!=",

                // Delimiters
                Comma => ",",
                Semicolon => ";",
                LeftParenthesis => "(",
                RightParenthesis => ")",
                LeftBrace => "{",
                RightBrace => "}",   

                EndOfLine => "\n",
                EndOfFile => "\n",

                t => panic!("not implemented! : {:?}", t)
            };
            write!(f, "{}", value)
        }
}


pub fn lookup_ident<'a>(ident: &'a str) -> Token<'a> {
    match ident {
        "fn" => Token::Function,
        "true" => Token::True,
        "false" => Token::False,
        "if" => Token::If,
        "else" => Token::Else,
        "return" => Token::Return,
        _ => Token::Ident(ident),
    }
}

#[test]
fn lookup_ident_test() {
    assert_eq!(lookup_ident("fn"), Token::Function);
}
