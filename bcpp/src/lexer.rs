use token;
use token::Token;

use std::char;

use std::str::Chars;
use std::iter::Peekable;

use std::str;
use std::str::FromStr;

pub struct Lexer<'a> {
    // input: Peekable<Chars<'a>>,
    input: &'a [u8],
    
    // lexing state
    position: usize,

    // string_vec: Vec<String>, // contain temporary string
}

impl<'a> Lexer<'a> {
    // pub fn new<I: Into<[u]>>(input: I) -> Lexer {
    //     Lexer { 
    //         input: input.as_bytes(),
    //         position: 0
    //     }
    // }
    pub fn new(input: &str) -> Lexer {
        Lexer { 
            input: input.as_bytes(),
            position: 0
        }
    }

    pub fn with_bytes(input: &'a [u8]) -> Lexer {
        Lexer { 
            input: input,
            position: 0
        }
    }

    fn read_char(&mut self) -> Option<&u8> {
        let ch = self.input.get(self.position);
        self.position += 1;
        ch
    }

    fn peek_char(&mut self) -> Option<&u8> {
        // self.input.peek()
        let ch = self.input.get(self.position);
        ch
    }

    fn peek_char_eq(&mut self, ch: u8) -> bool {
        match self.peek_char() {
            Some(&peek_ch) => peek_ch == ch,
            None => false,
        }
    }

    fn peek_char_eq_many(&mut self, chars: &[u8]) -> bool {
        for &ch in chars {
            match self.peek_char() {
                Some(&peek_ch) if peek_ch == ch => return true,
                None => return false,
                _ => continue,
            }
        }
        false
    }

    fn skip_whitespace(&mut self) {
        const VT: u8 = 11;
        const FF: u8 = 12;

        while let Some(&c) = self.peek_char() {
            if c != b' ' && c != b'\t' && c != b'\r' && c!= VT && c!= FF {
                break;
            }
            self.position += 1;
        }
    }

    fn peek_is_letter(&mut self) -> bool {
        match self.peek_char() {
            Some(&ch) => is_letter(ch),
            None => false,
        }
    }

    fn read_identifier(&mut self, begin: usize) -> &'a str {
        let mut end = begin + 1;
        while self.peek_is_letter() {
            self.read_char();
            end += 1;
        }

        let slice = str::from_utf8(&self.input[begin .. end]);
        slice.unwrap()
    }

    fn read_number(&mut self, begin: usize) -> &'a str {
        let mut end = begin + 1;
        while let Some(&c) = self.peek_char() {
            let ch = char::from_u32(c as u32).unwrap();
            if ch.is_numeric() == false {
                break;
            }
            self.read_char();
            end += 1;
        }

        let slice = str::from_utf8(&self.input[begin .. end]);
        slice.unwrap()
    }

    pub fn next_token(&mut self) -> Token<'a> {
        self.skip_whitespace();

        let current_pos = self.position;        
        match self.read_char() {
            Some(&ch) => match ch {
                b'#' => Token::Hash,
                b'$' => Token::Dollar,
                b'?' => Token::Question,
                b':' => Token::Colon,
                b';' => Token::Semicolon,
                b',' => Token::Comma,
                b'~' => Token::Tilde,

                b'=' => {
                    if self.peek_char_eq(b'=') {
                        self.read_char();
                        Token::Equal
                    } else {
                        Token::Assign
                    }
                }
                b'+' => {
                    if self.peek_char_eq_many(&[b'+', b'=']){
                        self.read_char();
                        Token::Other
                    }else{
                        Token::Plus
                    }
                }
                b'-' => {
                    if self.peek_char_eq_many(&[b'-', b'=', b'>']){
                        self.read_char();
                        Token::Other
                    }else{
                        Token::Minus
                    }
                }
                b'!' => {
                    if self.peek_char_eq(b'=') {
                        self.read_char();
                        Token::NotEqual
                    } else {
                        Token::Bang
                    }
                }

                b'&' => {
                    if self.peek_char_eq_many(&[b'&', b'=']){
                        self.read_char();
                        Token::Other
                    }else{
                        Token::Ampersand
                    }
                }
                b'|' => {
                    if self.peek_char_eq_many(&[b'|', b'=']){
                        self.read_char();
                        Token::Other
                    }else{
                        Token::Pipe
                    }
                }
                

                b'/' => Token::Slash,
                b'*' => Token::Asterisk,
                b'<' => Token::LowerThan,
                b'>' => Token::GreaterThan,

                b'{' => Token::LeftBrace,
                b'}' => Token::RightBrace,
                b'(' => Token::LeftParenthesis,
                b')' => Token::RightParenthesis,

                b'0' ... b'9' => {
                    let literal = self.read_number(current_pos);
                    Token::Integer(literal)
                },
                b'a' ... b'z' |
                b'A' ... b'Z' => {
                    let literal = self.read_identifier(current_pos);
                    Token::Ident(literal)
                },
                b'\n' => {
                    Token::EndOfLine
                },
                illegal_ch => {
                    println!("illegal char: {}", illegal_ch);
                    Token::Illegal // TODO: Maybe we need ch here, to display a nice error message later?
                }
            },

            // Handle EOF
            None => Token::EndOfFile,
        }
    }
}

// is_letter checks whether a char is a valid alphabetic character or an underscore
fn is_letter(ch: u8) -> bool {
    let ch = char::from_u32(ch as u32).unwrap();
    ch.is_alphabetic() || ch == '_'
}

#[cfg(test)]
mod testing {
    use super::*;

    #[test]
    fn is_letter_test() {
        assert!(is_letter(b'_'));
        assert!(is_letter(b'a'));
        assert!(is_letter(b'Z'));

        assert!(!is_letter(b'*'));
        assert!(!is_letter(b'1'));
    }


    #[test]
    fn test_only_whitespace(){
        let input = " \t\r";
        let mut lexer = Lexer::new(input);
        let token = lexer.next_token();
        assert_eq!(token, Token::EndOfFile);
    }

    #[test]
    fn test_token_plus(){
        let input = "+ ++ += ";
        let mut lexer = Lexer::new(input);
        assert_eq!(lexer.next_token(), Token::Plus);
        assert_eq!(lexer.next_token(), Token::Other);
        assert_eq!(lexer.next_token(), Token::Other);
        assert_eq!(lexer.next_token(), Token::EndOfFile);
    }

    #[test]
    fn test_token_minus(){
        let input = " - -- -= -> ";
        let mut lexer = Lexer::new(input);
        assert_eq!(lexer.next_token(), Token::Minus);
        assert_eq!(lexer.next_token(), Token::Other);
        assert_eq!(lexer.next_token(), Token::Other);
        assert_eq!(lexer.next_token(), Token::Other);
        assert_eq!(lexer.next_token(), Token::EndOfFile);
    }

    #[test]
    fn test_token_ampersand(){
        let input = " & && &= ";
        let mut lexer = Lexer::new(input);
        assert_eq!(lexer.next_token(), Token::Ampersand);
        assert_eq!(lexer.next_token(), Token::Other);
        assert_eq!(lexer.next_token(), Token::Other);
        assert_eq!(lexer.next_token(), Token::EndOfFile);
    }

    #[test]
    fn test_token_pipe(){
        let input = " | || |= ";
        let mut lexer = Lexer::new(input);
        assert_eq!(lexer.next_token(), Token::Pipe);
        assert_eq!(lexer.next_token(), Token::Other);
        assert_eq!(lexer.next_token(), Token::Other);
        assert_eq!(lexer.next_token(), Token::EndOfFile);
    }

    #[test]
    fn test_token_simple(){
        let input = " (),?~";
        let mut lexer = Lexer::new(input);
        assert_eq!(lexer.next_token(), Token::LeftParenthesis);
        assert_eq!(lexer.next_token(), Token::RightParenthesis);
        assert_eq!(lexer.next_token(), Token::Comma);
        assert_eq!(lexer.next_token(), Token::Question);
        assert_eq!(lexer.next_token(), Token::Tilde);
        assert_eq!(lexer.next_token(), Token::EndOfFile);
    }


    #[test]
    fn test_token_identifier(){
        let input = " FOO BAR";
        let mut lexer = Lexer::new(input);
        assert_eq!(lexer.next_token(), Token::Ident("FOO"));
        assert_eq!(lexer.next_token(), Token::Ident("BAR"));
        assert_eq!(lexer.next_token(), Token::EndOfFile);
    }

    #[test]
    fn test_token_end_of_line(){
        let input = " \n\r\nHELLO\n";
        let mut lexer = Lexer::new(input);
        assert_eq!(lexer.next_token(), Token::EndOfLine);
        assert_eq!(lexer.next_token(), Token::EndOfLine);
        assert_eq!(lexer.next_token(), Token::Ident("HELLO"));
        assert_eq!(lexer.next_token(), Token::EndOfLine);
        assert_eq!(lexer.next_token(), Token::EndOfFile);
    }
}