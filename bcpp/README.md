Arsitektur
----------

```
    -> Lexer::new() / Lexer::from_bytes()
       mengenerate Vec<Token>
     
    -> Vec<Token> lalu dikirim ke Parser::new()
       mengenerate CST (Concrete Syntax Tree)
       dari Node ( Directive, Line )
       
    -> CST Lalu diproses oleh Processor
       seperti Expander
       seperti Conditional
     
    
    LifeTime
    
    - Context
      - Lexer
        - Token
      - Parser
        - Node
      
    
    
```