use crate::ast::*;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Stmt {
    Expr(ExprStmt), // NOTE: change this to Expr ?
    Return(ReturnStmt),
    If(IfStmt),
    While(WhileStmt),
    DoWhile(DoWhileStmt),
    Compound(CompoundStmt),
    For(ForStmt),
    Goto(GotoStmt),
    Continue(ContinueStmt),
    Break(BreakStmt),
    Decl(DeclStmt),
    Switch(SwitchStmt),
    Case(CaseStmt),
    Default(DefaultStmt),
    Labeled(LabeledStmt),
    Empty(EmptyStmt),
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ExprStmt(pub Expr);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ReturnStmt(pub Option<Expr>);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct IfStmt(pub Expr, pub CompoundStmt, pub Option<CompoundStmt>);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct WhileStmt(pub Expr, pub CompoundStmt);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct DoWhileStmt(pub CompoundStmt, pub Expr);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ForStmt(pub Option<Expr>, pub Option<Expr>, pub Option<Expr>, pub CompoundStmt);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct SwitchStmt(pub Expr, pub CompoundStmt);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct CaseStmt(pub Expr, pub CompoundStmt);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct DefaultStmt(pub CompoundStmt);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct LabeledStmt(pub String, pub CompoundStmt);


#[derive(Debug, PartialEq, Eq, Clone)]
pub struct GotoStmt(pub Expr);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct BreakStmt();

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ContinueStmt();

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct EmptyStmt();


#[derive(Debug, PartialEq, Eq, Clone)]
pub struct CompoundStmt(pub Vec<Stmt>);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct DeclStmt(pub Type, pub String, pub Option<Expr>);



impl CompoundStmt {
    pub fn push(&mut self, stmt: Stmt) {
        self.0.push(stmt);
    }
}
