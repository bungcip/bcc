use crate::ast::*;


#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Type {
    Primitive(PrimitiveType),
    Name(String),
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum PrimitiveType {
    Char,
    Short,
    Int,
    Long,
    UChar,
    UShort,
    UInt,
    ULong,
    Float,
    Double,
    Void,
}


impl fmt::Display for Type {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let output = match self {
            &Type::Primitive(ref ty) => match ty {
                &PrimitiveType::Float => "float",
                &PrimitiveType::Double => "double",
                &PrimitiveType::Char => "char",
                &PrimitiveType::Short => "short",
                &PrimitiveType::Int => "int",
                &PrimitiveType::Long => "long",
                &PrimitiveType::UChar => "unsigned char",
                &PrimitiveType::UShort => "unsigned short",
                &PrimitiveType::UInt => "unsigned int",
                &PrimitiveType::ULong => "unsigned long",
                &PrimitiveType::Void => "void",
                // _ => unimplemented!()
            },
            &Type::Name(..) => unimplemented!()
        };
        write!(f, "{}", output)
    }
}

