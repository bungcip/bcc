use crate::ast::*;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct TranslationUnit {
    pub decls: Vec<Decl>
}

impl TranslationUnit {
    pub fn new(decls: Vec<Decl>) -> Self {
        TranslationUnit{decls}
    }
}