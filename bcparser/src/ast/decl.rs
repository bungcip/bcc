use crate::ast::Type;
use crate::ast::{Expr, Stmt};

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Decl {
    Struct(StructSpec),
    Union(UnionSpec),
    Enum(EnumSpec),
    Function(FunctionDecl),
    TypeDef(TypeDefDecl),
    FunctionDef(FunctionDef)
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct VarDecl(pub Type, pub String);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct FunctionDecl(pub Type, pub String, pub Vec<ParamDecl>);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ParamDecl(pub Type, pub String);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct TypeDefDecl(pub TypeSpec, pub String);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct FunctionDef(pub FunctionDecl, pub Vec<Stmt>);

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum TypeSpec {
    Struct(StructSpec),
    Union(UnionSpec),
    Enum(EnumSpec),
    Type(Type),
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct StructSpec(pub String, pub Vec<VarDecl>);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct UnionSpec(pub String, pub Vec<VarDecl>);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct EnumSpec(pub String, pub Vec<EnumMemberSpec>);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct EnumMemberSpec(pub String, pub Option<Expr>);



impl FunctionDef {
    pub fn name(&self) -> &str {
        &self. 0 . 1
    }

    pub fn ty(&self) -> &Type {
        &self. 0 . 0
    }
}
