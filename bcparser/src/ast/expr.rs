use crate::ast::Type;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum BinaryOp {
    Add,
    Subtract,
    Multiply,
    Divide,
    Remainder,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum UnaryOp {
    Positive,
    Negative,
    AddressOf,
    Deref,
    BitNot,
    Not,
}


#[derive(Debug, PartialEq, Eq, Clone)]
pub enum AssignmentOp {
    Assign,

    Add,
    Subtract,
    Multiply,
    Divide,
    Remainder,

    BitAnd,
    BitOr,
    BitXor,

    LeftShift,
    RightShift,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum AccessOp {
    Dot,
    Arrow,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum IncrementOp {
    Increment,
    Decrement,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum LogicalOp {
    And,
    Or,
}


#[derive(Debug, PartialEq, Eq, Clone)]
pub enum BitwiseOp {
    And,
    Or,
    Xor,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum CmpOp {
    LowerThan,
    GreaterThan,
    LowerThanEqual,
    GreaterThanEqual,
    Equal,
    NotEqual,
}



#[derive(Debug, PartialEq, Eq, Clone)]
pub enum IncrementMode {
    Postfix,
    Prefix,
}


#[derive(Debug, PartialEq, Eq, Clone)]
pub struct IntegerLit(pub usize);

#[derive(Debug, Clone)]
pub struct FloatLit(pub f64);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct CharLit(pub char);


#[derive(Debug, PartialEq, Eq, Clone)]
pub struct StringLit(pub String);


#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Expr {
    Name(String),

    Integer(IntegerLit),
    String(StringLit),
    Char(CharLit),
    Float(FloatLit),

    Binary(BinaryExpr),
    Assignment(AssignmentExpr),
    Call(CallExpr),
    SizeOf(SizeOfExpr),
    ArrayAccess(ArrayAccessExpr),
    Access(AccessExpr),
    Increment(IncrementExpr),
    Unary(UnaryExpr),
    Logical(LogicalExpr),
    Bitwise(BitwiseExpr),
    Cmp(CmpExpr),
    Elvis(ElvisExpr),
    Comma(CommaExpr),

    Cast(CastExpr)
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct BinaryExpr {
    pub op: BinaryOp,
    pub left: Box<Expr>,
    pub right: Box<Expr>
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct UnaryExpr {
    pub op: UnaryOp,
    pub expr: Box<Expr>,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct LogicalExpr {
    pub op: LogicalOp,
    pub left: Box<Expr>,
    pub right: Box<Expr>
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct BitwiseExpr {
    pub op: BitwiseOp,
    pub left: Box<Expr>,
    pub right: Box<Expr>
}


#[derive(Debug, PartialEq, Eq, Clone)]
pub struct AssignmentExpr {
    pub op: AssignmentOp,
    pub left: Box<Expr>,
    pub right: Box<Expr>
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct CmpExpr {
    pub op: CmpOp,
    pub left: Box<Expr>,
    pub right: Box<Expr>
}


#[derive(Debug, PartialEq, Eq, Clone)]
pub struct CallExpr {
    pub expr: Box<Expr>,
    pub args: Vec<Expr>
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum ExprOrType {
    Expr(Box<Expr>),
    Type(Type)
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct SizeOfExpr {
    pub expr_or_type: ExprOrType,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ArrayAccessExpr {
    pub left: Box<Expr>,
    pub right: Box<Expr>
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct AccessExpr {
    pub op: AccessOp,
    pub left: Box<Expr>,
    pub right: String
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct IncrementExpr {
    pub op: IncrementOp,
    pub mode: IncrementMode,
    pub expr: Box<Expr>
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ElvisExpr(pub Box<Expr>, pub Box<Expr>, pub Box<Expr>);

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct CommaExpr {
    pub left: Box<Expr>,
    pub right: Box<Expr>
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct CastExpr(pub Type, pub Box<Expr>);


impl PartialEq for FloatLit {
    fn eq(&self, other: &FloatLit) -> bool {
        use float_cmp::ApproxEqUlps;
        self.0.approx_eq_ulps(&other.0, 2)
    }
}
impl Eq for FloatLit {}


impl BinaryExpr {
    pub fn new(op: BinaryOp, left: Expr, right: Expr) -> Self {
        BinaryExpr {
            op: op,
            left: Box::new(left),
            right: Box::new(right),
        }
    }
}

impl UnaryExpr {
    pub fn new(op: UnaryOp, expr: Expr) -> Self {
        UnaryExpr {
            op: op,
            expr: Box::new(expr),
        }
    }
}


impl ArrayAccessExpr {
    pub fn new(left: Expr, right: Expr) -> Self {
        ArrayAccessExpr {
            left: Box::new(left),
            right: Box::new(right),
        }
    }
}

impl AccessExpr {
    pub fn new(op: AccessOp, left: Expr, right: String) -> Self {
        AccessExpr {
            op: op,
            left: Box::new(left),
            right: right,
        }
    }
}


impl AssignmentExpr {
    pub fn new(op: AssignmentOp, left: Expr, right: Expr) -> Self {
        AssignmentExpr {
            op: op,
            left: Box::new(left),
            right: Box::new(right),
        }
    }
}

impl CallExpr {
    pub fn new(expr: Expr, args: Vec<Expr>) -> Self {
        CallExpr {
            expr: Box::new(expr),
            args: args
        }
    }
}

impl SizeOfExpr {
    pub fn new(expr_or_type: ExprOrType) -> Self {
        SizeOfExpr { expr_or_type }
    }
}

impl IncrementExpr {
    pub fn new(op: IncrementOp, mode: IncrementMode, expr: Expr) -> Self {
        IncrementExpr {
            op: op,
            mode: mode,
            expr: Box::new(expr)
        }
    }
}

impl LogicalExpr {
    pub fn new(op: LogicalOp, left: Expr, right: Expr) -> Self {
        LogicalExpr {
            op: op,
            left: Box::new(left),
            right: Box::new(right),
        }
    }
}

impl BitwiseExpr {
    pub fn new(op: BitwiseOp, left: Expr, right: Expr) -> Self {
        BitwiseExpr {
            op: op,
            left: Box::new(left),
            right: Box::new(right),
        }
    }
}

impl CmpExpr {
    pub fn new(op: CmpOp, left: Expr, right: Expr) -> Self {
        CmpExpr {
            op: op,
            left: Box::new(left),
            right: Box::new(right),
        }
    }
}


impl ElvisExpr {
    pub fn new(expr: Expr, then_expr: Expr, else_expr: Expr) -> Self {
        ElvisExpr(Box::new(expr), Box::new(then_expr), Box::new(else_expr))
    }
}

impl CommaExpr {
    pub fn new(left: Expr, right: Expr) -> Self {
        CommaExpr {
            left: Box::new(left),
            right: Box::new(right),
        }
    }
}

impl CastExpr {
    pub fn new(ty: Type, expr: Expr) -> Self {
        CastExpr(ty, Box::new(expr))
    }
}