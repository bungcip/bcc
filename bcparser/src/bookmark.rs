use std::fmt;

//#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub type Offset = u32;

/// Bookmark contain the index of token location
pub struct Bookmark {
    last_offset: Offset,
    line_map: Vec<Offset>,
    // spans: Vec<Span>,
    // comment_spans: Vec<Span>,
}

/// location of token
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Pos {
    pub line: u16,
    pub column: u16,
}

/// Inclusive Range of Token/Node Position
#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Span {
    pub begin: Offset,
    pub end: Offset
}

//impl Into<Offset> for usize {
//    fn into(self) -> Offset {
//        self as u32
//    }
//}


impl Default for Pos {
    fn default() -> Self {
        Pos::new(1, 1)
    }
}

impl fmt::Display for Pos {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}:{}", self.line, self.column)
    }
}

impl Pos {
    pub fn new(line: u16, column: u16) -> Self {
        assert_ne!(line, 0);
        assert_ne!(column, 0);

        Pos { line, column }
    }
}

impl Span {
    pub fn new(begin: Offset, end: Offset) -> Span {
        Span { begin, end }
    }
}

impl Bookmark {
    pub fn new() -> Self {
        Bookmark {
            last_offset: 0,
            line_map: vec![0],
            // spans: vec![],
            // comment_spans: vec![],
        }
    }

    // pub fn set_spans(&mut self, spans: Vec<Span>){
    //     self.spans = spans;
    //     // self.comment_spans = comment_spans;
    // }

    pub fn mark(&mut self, offset: Offset) {
        assert!(self.last_offset < offset);

        self.last_offset = offset;
        self.line_map.push(offset);
    }

    // pub fn get_span(&self, index: usize) -> Option<&Span> {
    //     self.spans.get(index)
    // }

    pub fn last_line(&self) -> u32 {
        self.line_map.len() as u32
    }

    /// TODO: make it fast!!
    ///       use binary search instead of linear search
    pub fn decode_offset(&self, offset: Offset) -> Pos {
        let mut index = 1;
        for (line, line_offset) in self.line_map.iter().enumerate() {
            if *line_offset > offset {
                index = line;
                break;
            }
        }

        let current_offset = self.line_map[index - 1];
        let current_line = index as u32;
        let current_column = offset - current_offset + 1;

        Pos::new(current_line as u16, current_column as u16)
    }
}

#[cfg(test)]
mod testing {
    use super::*;

    #[test]
    fn test_decode_offset(){
        let mut bookmark = Bookmark::new();
        bookmark.mark(8);
        bookmark.mark(20);

        assert_eq!(bookmark.decode_offset(0), Pos::new(1,1));
        assert_eq!(bookmark.decode_offset(9), Pos::new(2,2));
    }
}