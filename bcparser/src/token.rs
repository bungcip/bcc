use soa_derive::StructOfArray;

use crate::{Offset};
use std::fmt;

bitflags! {
    #[derive(Debug, PartialEq, Copy, Clone, Eq)]
    pub struct IntegerSuffix: u8 {
        const SUFFIX_NONE = 0b000;
        const SUFFIX_U    = 0b001;
        const SUFFIX_L    = 0b010;
        const SUFFIX_LL   = 0b100;
    }
}

#[derive(Debug, PartialEq, Copy, Clone, Eq)]
#[repr(u8)]
pub enum FloatSuffix {
    None,
    Long,
    Float,
}

#[derive(Debug, PartialEq, Clone, Eq)]
#[repr(u8)]
pub enum NumberToken {
    Integer(String, IntegerSuffix),
    Float(String, FloatSuffix),
}

#[derive(Debug, PartialEq, Clone, Eq, Copy)]
#[repr(u8)]
pub enum Tag {
    Illegal,
    EndOfLine,
    EndOfFile,

    // Comment
    LineComment,
    BlockComment,

    // Literals are stored as strings
    Identifier,
    LiteralInteger,
    LiteralFloat,
    Character,
    String,

    // Operators
    Dollar,
    Question,
    Colon,
    DoubleColon,
    Tilde,
    Caret,
    Percent,

    // operator: binary
    Ampersand,
    Pipe,
    LeftShift,
    RightShift,

    Plus,
    Minus,
    Bang,
    Asterisk,
    Slash,
    Arrow,

    // operator: logical binary
    LogicalOr,
    LogicalAnd,

    // operator: assigment
    Assign,
    PlusAssign,
    MinusAssign,
    AsteriskAssign,
    SlashAssign,
    AmpersandAssign,
    PipeAssign,
    CaretAssign,
    PercentAssign,
    LeftShiftAssign,
    RightShiftAssign,

    // Operator: comparation
    LowerThan,
    LowerThanEqual,
    GreaterThan,
    GreaterThanEqual,
    Equal,
    NotEqual,

    Increment,
    Decrement,

    // Delimiters
    Dot,
    Comma,
    Semicolon,
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    LeftBracket,
    RightBracket,

    // Keywords
    Auto,
    Break,
    Case,
    Char,
    Const,
    Continue,
    Default,
    Do,
    Double,
    Else,
    Enum,
    Extern,
    Float,
    False,
    For,
    Goto,
    If,
    Int,
    Long,
    Register,
    Return,
    Short,
    Signed,
    Sizeof,
    Static,
    Struct,
    Switch,
    True,
    Typedef,
    Union,
    Unsigned,
    Void,
    Volatile,
    While,
}

#[derive(Debug, PartialEq, Clone, Copy, Eq)]
pub enum Keyword {
    Auto,
    Break,
    Case,
    Char,
    Const,
    Continue,
    Default,
    Do,
    Double,
    Else,
    Enum,
    Extern,
    Float,
    False,
    For,
    Goto,
    If,
    Int,
    Long,
    Register,
    Return,
    Short,
    Signed,
    Sizeof,
    Static,
    Struct,
    Switch,
    True,
    Typedef,
    Union,
    Unsigned,
    Void,
    Volatile,
    While,
}

impl<'a> Default for Tag {
    // Choose an Illegal identifier as default
    // this should be overriden before being used
    fn default() -> Tag {
        Tag::Illegal
    }
}

impl<'a> fmt::Display for Tag {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Tag::*;

        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        let value = match *self {
            Identifier => "{Identifier}",
            //            Integer(ref value) => &value,
            Dollar => "$",
            Question => "?",
            Colon => ":",
            Tilde => "~",

            Ampersand => "&",
            Pipe => "|",

            Assign => "=",
            Plus => "+",
            Minus => "-",
            Bang => "!",
            Asterisk => "*",
            Slash => "/",
            LowerThan => "<",
            GreaterThan => ">",
            Equal => "=",
            NotEqual => "!=",

            // Delimiters
            Comma => ",",
            Semicolon => ";",
            LeftParen => "(",
            RightParen => ")",
            LeftBrace => "{",
            RightBrace => "}",

            EndOfLine => "\n",
            EndOfFile => "\n",

            ref t => panic!("not implemented! : {:?}", t),
        };
        write!(f, "{}", value)
    }
}

impl Tag {
    /// return true if token is primitive or id
    pub fn is_type(&self) -> bool {
        match *self {
            Tag::Char
            | Tag::Short
            | Tag::Int
            | Tag::Long
            | Tag::Float
            | Tag::Double
            | Tag::Void
            | Tag::Unsigned
            | Tag::Identifier => true,
            _ => false,
        }
    }
}

/// get keyword from token
pub fn lookup_keyword(token: &Tag) -> Option<Keyword> {
    use self::Tag::*;

    match *token {
        Auto => Some(Keyword::Auto),
        Break => Some(Keyword::Break),
        Case => Some(Keyword::Case),
        Char => Some(Keyword::Char),
        Const => Some(Keyword::Const),
        Continue => Some(Keyword::Continue),
        Default => Some(Keyword::Default),
        Do => Some(Keyword::Do),
        Double => Some(Keyword::Double),
        Else => Some(Keyword::Else),
        Enum => Some(Keyword::Enum),
        Extern => Some(Keyword::Extern),
        Float => Some(Keyword::Float),
        False => Some(Keyword::False),
        For => Some(Keyword::For),
        Goto => Some(Keyword::Goto),
        If => Some(Keyword::If),
        Int => Some(Keyword::Int),
        Long => Some(Keyword::Long),
        Register => Some(Keyword::Register),
        Return => Some(Keyword::Return),
        Short => Some(Keyword::Short),
        Signed => Some(Keyword::Signed),
        Sizeof => Some(Keyword::Sizeof),
        Static => Some(Keyword::Static),
        Struct => Some(Keyword::Struct),
        Switch => Some(Keyword::Switch),
        True => Some(Keyword::True),
        Typedef => Some(Keyword::Typedef),
        Union => Some(Keyword::Union),
        Unsigned => Some(Keyword::Unsigned),
        Void => Some(Keyword::Void),
        Volatile => Some(Keyword::Volatile),
        While => Some(Keyword::While),
        _ => None,
    }
}

pub fn lookup_ident(identifier: &str) -> Tag {
    match identifier {
        "true" => Tag::True,
        "false" => Tag::False,
        "if" => Tag::If,
        "else" => Tag::Else,
        "return" => Tag::Return,
        _ => Tag::Identifier,
    }
}

#[derive(StructOfArray, Clone, Copy)]
pub struct Token {
    pub tag: Tag,
    pub start: Offset,
}

impl Token {
    pub fn new(tag: Tag, start: Offset) -> Self {
        Token { tag, start }
    }
}

