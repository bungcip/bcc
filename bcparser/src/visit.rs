use crate::ast::*;

pub trait Visitor {
    fn visit_translation_unit(&mut self, _node: &TranslationUnit)  { unimplemented!() }
    fn visit_expr(&mut self, node: &Expr)  {
//        unimplemented!()
        match node {
            &Expr::Name(ref name) => self.visit_id(name),
            &Expr::Integer(ref name) => self.visit_integer(name),
            &Expr::String(ref name) => self.visit_string(name),
            &Expr::Float(ref name) => self.visit_float(name),
            &Expr::Char(ref name) => self.visit_char(name),
            &Expr::Assignment(ref expr) => self.visit_assignment(expr),
            &Expr::Comma(ref expr) => self.visit_comma(expr),
            &Expr::Unary(ref expr) => self.visit_unary(expr),
            &Expr::Increment(ref expr) => self.visit_increment(expr),
            &Expr::Call(ref expr) => self.visit_call(expr),
            &Expr::SizeOf(ref expr) => self.visit_size_of(expr),
            &Expr::Access(ref expr) => self.visit_access(expr),
            &Expr::Binary(ref expr) => self.visit_binary(expr),
            &Expr::ArrayAccess(ref expr) => self.visit_array_access(expr),
            &Expr::Bitwise(ref expr) => self.visit_bitwise(expr),
            &Expr::Cmp(ref expr) => self.visit_cmp(expr),
            &Expr::Elvis(ref expr) => self.visit_elvis(expr),
            &Expr::Logical(ref expr) => self.visit_logical(expr),
            &Expr::Cast(ref expr) => self.visit_cast(expr),
        }
    }

    fn visit_id(&mut self, _node: &String)  { unimplemented!() }
    fn visit_integer(&mut self, _node: &IntegerLit)  { unimplemented!() }
    fn visit_string(&mut self, _node: &StringLit)  { unimplemented!() }
    fn visit_char(&mut self, _node: &CharLit)  { unimplemented!() }
    fn visit_float(&mut self, _node: &FloatLit)  { unimplemented!() }

    fn visit_binary(&mut self, node: &BinaryExpr)  { 
        self.visit_expr(&node.left);
        self.visit_expr(&node.right); 
    }

    fn visit_assignment(&mut self, _node: &AssignmentExpr)  { unimplemented!() }
    fn visit_call(&mut self, _node: &CallExpr)  { unimplemented!() }
    fn visit_size_of(&mut self, _node: &SizeOfExpr)  { unimplemented!() }
    fn visit_array_access(&mut self, _node: &ArrayAccessExpr)  { unimplemented!() }

    fn visit_access(&mut self, _node: &AccessExpr)  { unimplemented!() }
    fn visit_increment(&mut self, _node: &IncrementExpr)  { unimplemented!() }

    fn visit_unary(&mut self, _node: &UnaryExpr)  { unimplemented!() }
    fn visit_logical(&mut self, _node: &LogicalExpr)  { unimplemented!() }
    fn visit_bitwise(&mut self, _node: &BitwiseExpr)  { unimplemented!() }
    fn visit_cmp(&mut self, _node: &CmpExpr)  { unimplemented!() }
    fn visit_elvis(&mut self, _node: &ElvisExpr)  { unimplemented!() }
    fn visit_comma(&mut self, _node: &CommaExpr)  { unimplemented!() }

    fn visit_cast(&mut self, _node: &CastExpr)  { unimplemented!() }

    fn visit_stmt(&mut self, node: &Stmt)  {
        match node {
            &Stmt::Decl(ref stmt) => self.visit_decl_stmt(stmt),
            &Stmt::Default(ref stmt) => self.visit_default_stmt(stmt),
            &Stmt::Expr(ref stmt) => self.visit_expr_stmt(stmt),
            &Stmt::Switch(ref stmt) => self.visit_switch_stmt(stmt),
            &Stmt::Break(ref stmt) => self.visit_break_stmt(stmt),
            &Stmt::Continue(ref stmt) => self.visit_continue_stmt(stmt),
            &Stmt::Empty(ref stmt) => self.visit_empty_stmt(stmt),
            &Stmt::Return(ref stmt) => self.visit_return_stmt(stmt),
            &Stmt::Case(ref stmt) => self.visit_case_stmt(stmt),
            &Stmt::While(ref stmt) => self.visit_while_stmt(stmt),
            &Stmt::Goto(ref stmt) => self.visit_goto_stmt(stmt),
            &Stmt::For(ref stmt) => self.visit_for_stmt(stmt),
            &Stmt::Compound(ref stmt) => self.visit_compound_stmt(stmt),
            &Stmt::DoWhile(ref stmt) => self.visit_do_while_stmt(stmt),
            &Stmt::If(ref stmt) => self.visit_if_stmt(stmt),
            &Stmt::Labeled(ref stmt) => self.visit_labeled_stmt(stmt),
        }
    }

    fn visit_expr_stmt(&mut self, node: &ExprStmt)  {
        self.visit_expr(&node.0)
    }

    fn visit_return_stmt(&mut self, _node: &ReturnStmt)  { unimplemented!() }
    fn visit_if_stmt(&mut self, _node: &IfStmt)  { unimplemented!() }
    fn visit_do_while_stmt(&mut self, _node: &DoWhileStmt)  { unimplemented!() }
    fn visit_while_stmt(&mut self, _node: &WhileStmt)  { unimplemented!() }
    fn visit_compound_stmt(&mut self, _node: &CompoundStmt)  { unimplemented!() }
    fn visit_for_stmt(&mut self, _node: &ForStmt)  { unimplemented!() }
    fn visit_goto_stmt(&mut self, _node: &GotoStmt)  { unimplemented!() }
    fn visit_continue_stmt(&mut self, _node: &ContinueStmt)  { unimplemented!() }
    fn visit_break_stmt(&mut self, _node: &BreakStmt)  { unimplemented!() }
    fn visit_decl_stmt(&mut self, _node: &DeclStmt)  { unimplemented!() }
    fn visit_switch_stmt(&mut self, _node: &SwitchStmt)  { unimplemented!() }
    fn visit_case_stmt(&mut self, _node: &CaseStmt)  { unimplemented!() }
    fn visit_default_stmt(&mut self, _node: &DefaultStmt)  { unimplemented!() }
    fn visit_labeled_stmt(&mut self, _node: &LabeledStmt)  { unimplemented!() }
    fn visit_empty_stmt(&mut self, _node: &EmptyStmt)  { unimplemented!() }

    fn visit_decl(&mut self, node: &Decl)  {
        match node {
            &Decl::FunctionDef(ref decl) => self.visit_function_def(decl),
            &Decl::Function(ref decl) => self.visit_function_decl(decl),
            &Decl::Struct(ref decl) => self.visit_struct_decl(decl),
            &Decl::Enum(ref decl) => self.visit_enum_decl(decl),
            &Decl::Union(ref decl) => self.visit_union_decl(decl),
            &Decl::TypeDef(ref decl) => self.visit_typedef_decl(decl),
        }
    }

    fn visit_struct_decl(&mut self, _node: &StructSpec)  { unimplemented!() }
    fn visit_union_decl(&mut self, _node: &UnionSpec)  { unimplemented!() }
    fn visit_function_decl(&mut self, _node: &FunctionDecl)  { unimplemented!() }
    fn visit_typedef_decl(&mut self, _node: &TypeDefDecl)  { unimplemented!() }
    fn visit_function_def(&mut self, _node: &FunctionDef)  { unimplemented!() }

    fn visit_enum_decl(&mut self, _node: &EnumSpec)  { unimplemented!() }
    fn visit_enum_member_spec(&mut self, _node: &EnumMemberSpec)  { unimplemented!() }

    fn visit_type(&mut self, _node: &Type)  { unimplemented!() }
}


pub trait Visitable {
    fn accept<V: Visitor>(&self, visitor: &mut V);
}


//macro_rules! impl_visitable {
//    ($name:ty, $visitor_method:tt) => {
//        impl Visitable for $name {
//            fn accept<V: Visitor>(&self, visitor: &mut V) {
//                visitor.$visitor_method(self)
//            }
//        }
//    };
//}

//impl_visitable!(TranslationUnit, visit_translation_unit);
//
//impl_visitable!(String, visit_id);
//impl_visitable!(IntegerLit, visit_integer);
//impl_visitable!(StringLit, visit_string);
//impl_visitable!(CharLit, visit_char);
//impl_visitable!(FloatLit, visit_float);
//
//impl_visitable!(BinaryExpr, visit_binary);
//impl_visitable!(AssignmentExpr, visit_assignment);


impl Visitable for Decl {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        match self {
            &Decl::FunctionDef(ref decl) => visitor.visit_function_def(decl),
            &Decl::Function(ref decl) => visitor.visit_function_decl(decl),
            &Decl::Struct(ref decl) => visitor.visit_struct_decl(decl),
            &Decl::Enum(ref decl) => visitor.visit_enum_decl(decl),
            &Decl::Union(ref decl) => visitor.visit_union_decl(decl),
            &Decl::TypeDef(ref decl) => visitor.visit_typedef_decl(decl),
        }
    }
}

impl Visitable for Expr {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        match self {
            &Expr::Name(ref name) => visitor.visit_id(name),
            &Expr::Integer(ref name) => visitor.visit_integer(name),
            &Expr::String(ref name) => visitor.visit_string(name),
            &Expr::Float(ref name) => visitor.visit_float(name),
            &Expr::Char(ref name) => visitor.visit_char(name),
            &Expr::Assignment(ref expr) => visitor.visit_assignment(expr),
            &Expr::Comma(ref expr) => visitor.visit_comma(expr),
            &Expr::Unary(ref expr) => visitor.visit_unary(expr),
            &Expr::Increment(ref expr) => visitor.visit_increment(expr),
            &Expr::Call(ref expr) => visitor.visit_call(expr),
            &Expr::SizeOf(ref expr) => visitor.visit_size_of(expr),
            &Expr::Access(ref expr) => visitor.visit_access(expr),
            &Expr::Binary(ref expr) => visitor.visit_binary(expr),
            &Expr::ArrayAccess(ref expr) => visitor.visit_array_access(expr),
            &Expr::Bitwise(ref expr) => visitor.visit_bitwise(expr),
            &Expr::Cmp(ref expr) => visitor.visit_cmp(expr),
            &Expr::Elvis(ref expr) => visitor.visit_elvis(expr),
            &Expr::Logical(ref expr) => visitor.visit_logical(expr),
            &Expr::Cast(ref expr) => visitor.visit_cast(expr)
        }
    }
}

impl Visitable for ReturnStmt {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        match self.0 {
            Some(ref expr) => visitor.visit_expr(expr),
            None => ()
        }
    }
}

impl Visitable for CallExpr {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        visitor.visit_expr(&self.expr);
        for child in &self.args {
            visitor.visit_expr(child);
        }
    }
}

impl Visitable for BinaryExpr {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        visitor.visit_expr(&self.left);
        visitor.visit_expr(&self.right);
    }
}

//fn visit_binary(&mut self, node: &BinaryExpr)  { unimplemented!() }
//fn visit_assignment(&mut self, node: &AssignmentExpr)  { unimplemented!() }
//fn visit_call(&mut self, node: &CallExpr)  { unimplemented!() }
//fn visit_size_of(&mut self, node: &SizeOfExpr)  { unimplemented!() }
//fn visit_array_access(&mut self, node: &ArrayAccessExpr)  { unimplemented!() }
//
//fn visit_access(&mut self, node: &AccessExpr)  { unimplemented!() }
//fn visit_increment(&mut self, node: &IncrementExpr)  { unimplemented!() }
//
//fn visit_unary(&mut self, node: &UnaryExpr)  { unimplemented!() }
//fn visit_logical(&mut self, node: &LogicalExpr)  { unimplemented!() }
//fn visit_bitwise(&mut self, node: &BitwiseExpr)  { unimplemented!() }
//fn visit_cmp(&mut self, node: &CmpExpr)  { unimplemented!() }
//fn visit_elvis(&mut self, node: &ElvisExpr)  { unimplemented!() }
//fn visit_comma(&mut self, node: &CommaExpr)  { unimplemented!() }
//
//fn visit_stmt(&mut self, node: &Stmt)  { unimplemented!() }
//fn visit_expr_stmt(&mut self, node: &ExprStmt)  { unimplemented!() }
//fn visit_return_stmt(&mut self, node: &ReturnStmt)  { unimplemented!() }
//fn visit_if_stmt(&mut self, node: &IfStmt)  { unimplemented!() }
//fn visit_while_stmt(&mut self, node: &WhileStmt)  { unimplemented!() }
//fn visit_compound_stmt(&mut self, node: &CompoundStmt)  { unimplemented!() }
//fn visit_for_stmt(&mut self, node: &ForStmt)  { unimplemented!() }
//fn visit_goto_stmt(&mut self, node: &GotoStmt)  { unimplemented!() }
//fn visit_continue_stmt(&mut self, node: &ContinueStmt)  { unimplemented!() }
//fn visit_break_stmt(&mut self, node: &BreakStmt)  { unimplemented!() }
//fn visit_decl_stmt(&mut self, node: &DeclStmt)  { unimplemented!() }
//fn visit_switch_stmt(&mut self, node: &SwitchStmt)  { unimplemented!() }
//fn visit_case_stmt(&mut self, node: &CaseStmt)  { unimplemented!() }
//fn visit_default_stmt(&mut self, node: &DefaultStmt)  { unimplemented!() }
//fn visit_labeled_stmt(&mut self, node: &LabeledStmt)  { unimplemented!() }
//fn visit_empty_stmt(&mut self, node: &EmptyStmt)  { unimplemented!() }
//
//fn visit_struct_decl(&mut self, node: &StructSpec)  { unimplemented!() }
//fn visit_union_decl(&mut self, node: &UnionSpec)  { unimplemented!() }
//fn visit_function_decl(&mut self, node: &FunctionDecl)  { unimplemented!() }
//fn visit_typedef_decl(&mut self, node: &TypeDefDecl)  { unimplemented!() }
//fn visit_function_def(&mut self, node: &FunctionDef)  { unimplemented!() }
//
//fn visit_enum_decl(&mut self, node: &EnumSpec)  { unimplemented!() }
//fn visit_enum_member_spec(&mut self, node: &EnumMemberSpec)  { unimplemented!() }
//
//fn visit_type(&mut self, node: &Type)  { unimplemented!() }
