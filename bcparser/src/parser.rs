use std::fmt;

use crate::token::*;
use crate::ast::*;
use crate::Bookmark;

pub type ParseError = String;
pub type ParseResult<T> = Result<T, ParseError>;

pub trait Stream {
    type Item: fmt::Debug + PartialEq;

    fn has_next(&self) -> bool;
    fn next(&mut self) -> Option<Self::Item>;
    fn peek(&self) -> Option<Self::Item>;
    fn peek_again(&self) -> Option<Self::Item>;

    fn try_next(&mut self) -> ParseResult<Self::Item> {
        match self.next() {
            None => Err(String::from("Unexpected End of Stream")),
            Some(token) => Ok(token)
        }
    }

    fn try_peek(&self) -> ParseResult<Self::Item> {
        match self.peek() {
            None => Err(String::from("Unexpected End of Stream")),
            Some(token) => Ok(token)
        }
    }

    fn consume(&mut self, expected: &Self::Item) -> ParseResult<()> {
        match self.try_next()? {
            ref token if token == expected => Ok(()),
            ref token => Err(
                format!("expect {:?} but got {:?} instead", expected, token)
            ),
        }
    }

    fn accept(&mut self, expected: &Self::Item) -> bool {
        match self.peek() {
            None => false,
            Some(ref token) if token == expected => {
                self.next();
                true
            },
            Some(_) => false
        }
    }
}

#[derive(Clone)]
pub struct Parser {
    input: Vec<Tag>,
    position: usize,
    marked_position: usize,
}

/// parse a series of token as translation unit
pub fn parse(input: Vec<Tag>, _: Bookmark) -> ParseResult<TranslationUnit> {
    // TODO: do something to bookmark
    let mut parser = Parser::new(input);
    let tu = parser.translation_unit()?;
    Ok(tu)
}


impl Stream for Parser {
    type Item = Tag;

    fn has_next(&self) -> bool {
        self.position < self.input.len()
    }

    fn next(&mut self) -> Option<Tag> {
        let token = self.input.get(self.position);
        self.position += 1;
        token.cloned()
    }

    fn peek(&self) -> Option<Tag> {
        self.input.get(self.position).cloned()
    }
    fn peek_again(&self) -> Option<Tag> {
        self.input.get(self.position + 1).cloned()
    }
}

impl Parser {
    pub fn new(input: Vec<Tag>) -> Self {
        Parser {
            input: input,
            position: 0,
            marked_position: 0,
        }
    }

    /// parse as translation unit
    pub fn translation_unit(&mut self) -> ParseResult<TranslationUnit> {
        let mut decls = vec![];
        loop {
            if self.has_next() == false {
                break;
            }

            let decl = parse_decl(self)?;
            decls.push(decl);
        }

        let tu = TranslationUnit::new(decls);
        Ok(tu)
    }

    /// mark current stream position so we can rewind to this position later when required
    pub fn mark(&mut self){
        self.marked_position = self.position;
    }

    /// rewind current stream position to previous marked location
    pub fn rewind(&mut self){
        self.position = self.marked_position;
    }
}




/// Get precendence for spesific token
/// lower number mean lower precendence
fn get_precedence(token: &Tag) -> u8 {
    match *token {
        Tag::Comma => 50,
        Tag::Assign
        | Tag::PlusAssign
        | Tag::MinusAssign
        | Tag::SlashAssign
        | Tag::AsteriskAssign
        | Tag::PercentAssign
        | Tag::AmpersandAssign
        | Tag::CaretAssign
        | Tag::PipeAssign
        | Tag::LeftShiftAssign
        | Tag::RightShiftAssign => 70,
        Tag::Question => 100,

        Tag::LogicalOr => 110,
        Tag::LogicalAnd => 111,

        Tag::Pipe => 120,
        Tag::Caret => 121,
        Tag::Ampersand => 122,

        Tag::Equal | Tag::NotEqual => 130,
        Tag::LowerThan | Tag::LowerThanEqual | Tag::GreaterThan | Tag::GreaterThanEqual  => 140,

        Tag::LeftShift | Tag::RightShift => 150,

        Tag::Plus | Tag::Minus => 160,
        Tag::Asterisk | Tag::Slash | Tag::Percent => 170,

        Tag::LeftParen => 180,
        Tag::LeftBracket => 180,
        Tag::Dot | Tag::Arrow => 180,
        Tag::Increment | Tag::Decrement => 180,
        _ => 0,
    }
}

/// run action between surrounded Token, use this helper function
/// for expression or statement like if, switch, etc
fn surround<F, R>(stream: &mut Parser, start: &Tag, finish: &Tag, action: F)
                  -> ParseResult<R>
    where F: FnOnce(&mut Parser) -> ParseResult<R>
{
    stream.consume(start)?;
    let result = action(stream)?;
    stream.consume(finish)?;
    Ok(result)
}

fn surround_paren<F, R>(stream: &mut Parser, action: F) -> ParseResult<R>
    where F: FnOnce(&mut Parser) -> ParseResult<R>
{
    surround(stream, &Tag::LeftParen, &Tag::RightParen, action)
}


/// Get String from Token Identifier
fn name(stream: &mut Parser) -> ParseResult<String> {
    match stream.try_next()? {
        Tag::Identifier(id) => Ok(id),
        token => Err(format!("unexpected token = {:?}", token)),
    }
}

/// doing action between token start & finish separated with delimiter token
fn group_with_delimiter<F, R>(stream: &mut Parser, start: &Tag, finish: &Tag, delimiter: &Tag, action: F)
    -> ParseResult<Vec<R>>
    where F: Fn(&mut Parser) -> ParseResult<R>
{
    surround(stream, start, finish, |stream| {
        let mut result = vec![];
        loop {
            if stream.try_peek()? == *finish {
                break;
            }

            let expr = action(stream)?;
            result.push(expr);

            if stream.accept(delimiter) == false {
                break;
            }
        }

        Ok(result)
    })
}

/// doing action between token start & finish
fn group<F, R>(stream: &mut Parser, start: &Tag, finish: &Tag, action: F)  -> ParseResult<Vec<R>>
    where F: Fn(&mut Parser) -> ParseResult<R>
{
    surround(stream, start, finish, |stream| {
        let mut result = vec![];
        loop {
            if stream.try_peek()? == *finish {
                break;
            }

            let expr = action(stream)?;
            result.push(expr);
        }

        Ok(result)
    })
}


fn optional<F, R>(stream: &mut Parser, predicate: &Tag, action: F) -> ParseResult<Option<R>>
  where F: FnOnce(&mut Parser) -> ParseResult<R>
{
    if stream.accept(predicate) == false {
        return Ok(None);
    }

    let result = action(stream)?;
    Ok(Some(result))
}

fn optional_with_suffix<F, R>(stream: &mut Parser, suffix: &Tag, action: F) -> ParseResult<Option<R>>
    where F: FnOnce(&mut Parser) -> ParseResult<R>
{
    if stream.accept(suffix) {
        return Ok(None);
    }

    let result = action(stream)?;
    stream.consume(suffix)?;
    Ok(Some(result))
}


fn parse_prefix_expr(stream: &mut Parser) -> ParseResult<Expr> {
    //    println!("parse prefix: {}", stream.position);
    match stream.try_peek()? {
        Tag::Number(..) => parse_number_literal(stream),
        Tag::String(..) => parse_string_literal(stream),
        Tag::Character(..) => parse_char_literal(stream),
        Tag::LeftParen => parse_cast_or_paren_expr(stream),
        Tag::Sizeof => parse_sizeof_expr(stream),
        Tag::Increment | Tag::Decrement => parse_prefix_increment_expr(stream),
        Tag::Ampersand |
        Tag::Asterisk |
        Tag::Plus |
        Tag::Minus |
        Tag::Tilde |
        Tag::Bang => parse_unary_expr(stream),
        Tag::Identifier(..) => parse_name_expr(stream),
        t => Err(format!("parse_prefix: unexpected token: {:?}", t))
    }
}

macro_rules! wrap_result {
    ($expr: expr, $ret: expr) => {{
        let stmt = match $expr {
            Ok(stmt) => stmt,
            Err(why) => return Some(Err(why)),
        };
        let stmt = $ret(stmt);
        Some(Ok(stmt))
    }};
}

fn parse_prefix_stmt(stream: &mut Parser) -> Option<ParseResult<Stmt>> {
    match stream.peek() {
        Some(Tag::Return) => wrap_result!(parse_return_stmt(stream), Stmt::Return),
        Some(Tag::If) => wrap_result!(parse_if_stmt(stream), Stmt::If),
        Some(Tag::While) => wrap_result!(parse_while_stmt(stream), Stmt::While),
        Some(Tag::Do) => wrap_result!(parse_do_while_stmt(stream), Stmt::DoWhile),
        Some(Tag::For) => wrap_result!(parse_for_stmt(stream), Stmt::For),
        Some(Tag::Switch) => wrap_result!(parse_switch_stmt(stream), Stmt::Switch),
        Some(Tag::Case) => wrap_result!(parse_case_stmt(stream), Stmt::Case),
        Some(Tag::Default) => wrap_result!(parse_default_stmt(stream), Stmt::Default),
        Some(Tag::Goto) => wrap_result!(parse_goto_stmt(stream), Stmt::Goto),
        Some(Tag::Continue) => wrap_result!(parse_continue_stmt(stream), Stmt::Continue),
        Some(Tag::Break) => wrap_result!(parse_break_stmt(stream), Stmt::Break),
        Some(Tag::Int) | Some(Tag::Long) | Some(Tag::Short) | Some(Tag::Char) |
        Some(Tag::Float) | Some(Tag::Double) |
        Some(Tag::Unsigned) |
        Some(Tag::Void) |
        Some(Tag::Const) | Some(Tag::Static) => wrap_result!(parse_decl_stmt(stream), Stmt::Decl),
        Some(Tag::Semicolon) =>  wrap_result!(parse_empty_stmt(stream), Stmt::Empty),
        Some(Tag::Identifier(_)) => {
            // peek again
            match stream.peek_again() {
                Some(Tag::Colon) => wrap_result!(parse_labeled_stmt(stream), Stmt::Labeled),
                Some(_) | None => None,
            }
        },
        Some(_) | None => None,
    }
}


fn parse_infix_expr(stream: &mut Parser, left: Expr, precedence: u8) -> ParseResult<Expr> {
//    println!("parse infix: {}", stream.position);
    match stream.try_peek()? {
        Tag::Plus
        | Tag::Minus
        | Tag::Slash
        | Tag::Asterisk
        | Tag::Percent => {
            let expr = parse_binary_expr(stream, left, precedence)?;
            Ok(Expr::Binary(expr))
        },
        Tag::Assign
        | Tag::PlusAssign
        | Tag::MinusAssign
        | Tag::SlashAssign
        | Tag::AsteriskAssign
        | Tag::PercentAssign
        | Tag::AmpersandAssign
        | Tag::CaretAssign
        | Tag::PipeAssign
        | Tag::LeftShiftAssign
        | Tag::RightShiftAssign => {
            let expr = parse_assign_expr(stream, left, precedence)?;
            Ok(Expr::Assignment(expr))
        },
        Tag::LeftParen => {
            let expr = parse_call_expr(stream, left, precedence)?;
            Ok(Expr::Call(expr))
        },
        Tag::LeftBracket => {
            let expr = parse_array_access_expr(stream, left, precedence)?;
            Ok(Expr::ArrayAccess(expr))
        },
        Tag::Dot | Tag::Arrow => {
            let expr = parse_access_expr(stream, left, precedence)?;
            Ok(Expr::Access(expr))
        },
        Tag::Increment | Tag::Decrement => {
            let expr = parse_postfix_increment_expr(stream, left, precedence)?;
            Ok(Expr::Increment(expr))
        },
        Tag::LogicalAnd | Tag::LogicalOr => {
            let expr = parse_logical_expr(stream, left, precedence)?;
            Ok(Expr::Logical(expr))
        },
        Tag::Ampersand | Tag::Pipe | Tag::Caret => {
            let expr = parse_bitwise_expr(stream, left, precedence)?;
            Ok(Expr::Bitwise(expr))
        },
        Tag::LowerThan | Tag::LowerThanEqual | Tag::GreaterThan | Tag::GreaterThanEqual | Tag::Equal | Tag::NotEqual => {
            let expr = parse_cmp_expr(stream, left, precedence)?;
            Ok(Expr::Cmp(expr))
        },
        Tag::Question => {
            let expr = parse_elvis_expr(stream, left, precedence)?;
            Ok(Expr::Elvis(expr))
        },
        Tag::Comma => {
            let expr = parse_comma_expr(stream, left, precedence)?;
            Ok(Expr::Comma(expr))
        },
        t => Err(format!("parse_infix: Unexpected token {:?}", t))
    }
}

fn parse_infix_stmt(stream: &mut Parser, left: Expr) -> ParseResult<Stmt> {
    stream.try_peek()?;
    Ok(Stmt::Expr(parse_expr_stmt(stream, left)?))
}

fn parse_expr(stream: &mut Parser) -> ParseResult<Expr> {
    parse_expr_with_precedence(stream, 0)
}

fn parse_expr_with_precedence(stream: &mut Parser, precedence: u8) -> ParseResult<Expr> {
//    println!("parse expr: {}", stream.position);
    let mut expr = parse_prefix_expr(stream)?;
    while let Some(next_token) = stream.peek() {
//        println!("op token: {:?}", next_token);
        let next_precedence = get_precedence(&next_token);
        if precedence >= next_precedence {
            break;
        }

        expr = parse_infix_expr(stream, expr, next_precedence)?;
    }

    Ok(expr)
}

fn parse_stmt(stream: &mut Parser) -> ParseResult<Stmt> {
    let stmt = parse_prefix_stmt(stream);
    if let Some(stmt) = stmt {
        return stmt;
    }

    // infix
    let left = parse_expr(stream)?;
    return parse_infix_stmt(stream, left);
}

fn parse_expr_stmt(stream: &mut Parser, left: Expr) -> ParseResult<ExprStmt> {
    stream.consume(&Tag::Semicolon)?;
    Ok(ExprStmt(left))
}

fn parse_return_stmt(stream: &mut Parser) -> ParseResult<ReturnStmt> {
    stream.consume(&Tag::Return)?;
    let expr = optional_with_suffix(stream, &Tag::Semicolon, parse_expr)?;
    Ok(ReturnStmt(expr))
}

fn compound_stmt(stream: &mut Parser) -> ParseResult<CompoundStmt> {
    let compound = if stream.accept(&Tag::LeftBrace) {
        let mut result = vec![];
        loop {
            match stream.try_peek()? {
                Tag::RightBrace => break,
                _ => {
                    let expr = parse_stmt(stream)?;
                    result.push(expr);
                }
            }
        }
        result
    } else {
        let stmt = parse_stmt(stream)?;
        vec![stmt]
    };

    let compound = CompoundStmt(compound);
    Ok(compound)
}

fn parse_if_stmt(stream: &mut Parser) -> ParseResult<IfStmt> {
    stream.consume(&Tag::If)?;

    let expr = surround_paren(stream, parse_expr)?;
    let then_compound = compound_stmt(stream)?;
    let else_compound = optional(stream, &Tag::Else, compound_stmt)?;
    Ok(IfStmt(expr, then_compound, else_compound))
}

fn parse_while_stmt(stream: &mut Parser) -> ParseResult<WhileStmt> {
    stream.consume(&Tag::While)?;

    let expr = surround_paren(stream, parse_expr)?;
    let then = compound_stmt(stream)?;

    Ok(WhileStmt(expr, then))
}

fn parse_do_while_stmt(stream: &mut Parser) -> ParseResult<DoWhileStmt> {
    stream.consume(&Tag::Do)?;
    let then = compound_stmt(stream)?;

    stream.consume(&Tag::While)?;
    let expr = surround_paren(stream, parse_expr)?;

    Ok(DoWhileStmt(then, expr))
}

fn parse_for_condition_fragment(stream: &mut Parser) -> ParseResult<Option<Expr>> {
    let expr = match stream.try_peek()? {
        Tag::Semicolon | Tag::RightParen => None,
        _ => Some(parse_expr(stream)?)
    };

    Ok(expr)
}

fn parse_for_stmt(stream: &mut Parser) -> ParseResult<ForStmt> {
    stream.consume(&Tag::For)?;

    let fragments = group_with_delimiter(stream,
         &Tag::LeftParen, &Tag::RightParen,
         &Tag::Semicolon, parse_for_condition_fragment)?;

    if fragments.len() != 3 {
        return Err(format!("Expected 3 for condition fragment only"));
    }

    let mut fragments = fragments.into_iter();
    let expr1 = fragments.next().unwrap();
    let expr2 = fragments.next().unwrap();
    let expr3 = fragments.next().unwrap();

    let then = compound_stmt(stream)?;
    Ok(ForStmt(expr1, expr2, expr3, then))
}


fn parse_switch_stmt(stream: &mut Parser) -> ParseResult<SwitchStmt> {
    stream.consume(&Tag::Switch)?;

    let expr = surround_paren(stream, parse_expr)?;
    let then = compound_stmt(stream)?;
    
    Ok(SwitchStmt(expr, then))
}

fn parse_case_stmt(stream: &mut Parser) -> ParseResult<CaseStmt> {
    stream.consume(&Tag::Case)?;
    let expr = parse_expr(stream)?;
    stream.consume(&Tag::Colon)?;
    let then = compound_stmt(stream)?;

    Ok(CaseStmt(expr, then))
}

fn parse_default_stmt(stream: &mut Parser) -> ParseResult<DefaultStmt> {
    stream.consume(&Tag::Default)?;
    stream.consume(&Tag::Colon)?;
    let then = compound_stmt(stream)?;

    Ok(DefaultStmt(then))
}

fn parse_labeled_stmt(stream: &mut Parser) -> ParseResult<LabeledStmt> {
    let name = name(stream)?;
    stream.consume(&Tag::Colon)?;
    let then = compound_stmt(stream)?;

    Ok(LabeledStmt(name, then))
}


fn parse_goto_stmt(stream: &mut Parser) -> ParseResult<GotoStmt> {
    stream.consume(&Tag::Goto)?;
    let expr = parse_name_expr(stream)?;
    stream.consume(&Tag::Semicolon)?;

    Ok(GotoStmt(expr))
}

fn parse_continue_stmt(stream: &mut Parser) -> ParseResult<ContinueStmt> {
    stream.consume(&Tag::Continue)?;
    stream.consume(&Tag::Semicolon)?;
    Ok(ContinueStmt())
}

fn parse_break_stmt(stream: &mut Parser) -> ParseResult<BreakStmt> {
    stream.consume(&Tag::Break)?;
    stream.consume(&Tag::Semicolon)?;
    Ok(BreakStmt())
}

fn parse_empty_stmt(stream: &mut Parser) -> ParseResult<EmptyStmt> {
    stream.consume(&Tag::Semicolon)?;
    Ok(EmptyStmt())
}


fn expect_binary_op(stream: &mut Parser) -> ParseResult<BinaryOp> {
    match stream.try_next()? {
        Tag::Plus => Ok(BinaryOp::Add),
        Tag::Minus => Ok(BinaryOp::Subtract),
        Tag::Asterisk => Ok(BinaryOp::Multiply),
        Tag::Slash => Ok(BinaryOp::Divide),
        token => Err(format!("expect_operator: fail! token = {:?}", token)),
    }
}

fn expect_logical_op(stream: &mut Parser) -> ParseResult<LogicalOp> {
    match stream.try_next()? {
        Tag::LogicalOr => Ok(LogicalOp::Or),
        Tag::LogicalAnd => Ok(LogicalOp::And),
        token => Err(format!("expect_operator: fail! token = {:?}", token)),
    }
}

fn expect_bitwise_op(stream: &mut Parser) -> ParseResult<BitwiseOp> {
    match stream.try_next()? {
        Tag::Pipe => Ok(BitwiseOp::Or),
        Tag::Ampersand => Ok(BitwiseOp::And),
        Tag::Caret => Ok(BitwiseOp::Xor),
        token => Err(format!("expect_operator: fail! token = {:?}", token)),
    }
}

fn expect_cmp_op(stream: &mut Parser) -> ParseResult<CmpOp> {
    match stream.try_next()? {
        Tag::LowerThan => Ok(CmpOp::LowerThan),
        Tag::LowerThanEqual => Ok(CmpOp::LowerThanEqual),
        Tag::GreaterThan => Ok(CmpOp::GreaterThan),
        Tag::GreaterThanEqual => Ok(CmpOp::GreaterThanEqual),
        Tag::Equal => Ok(CmpOp::Equal),
        Tag::NotEqual => Ok(CmpOp::NotEqual),
        token => Err(format!("expect_operator: fail! token = {:?}", token)),
    }
}


fn expect_access_op(stream: &mut Parser) -> ParseResult<AccessOp> {
    match stream.try_next()? {
        Tag::Dot => Ok(AccessOp::Dot),
        Tag::Arrow => Ok(AccessOp::Arrow),
        token => Err(format!("expect_operator: fail! token = {:?}", token)),
    }
}

fn expect_unary_op(stream: &mut Parser) -> ParseResult<UnaryOp> {
    match stream.try_next()? {
        Tag::Plus => Ok(UnaryOp::Positive),
        Tag::Minus => Ok(UnaryOp::Negative),
        Tag::Asterisk => Ok(UnaryOp::Deref),
        Tag::Ampersand => Ok(UnaryOp::AddressOf),
        Tag::Tilde => Ok(UnaryOp::BitNot),
        Tag::Bang => Ok(UnaryOp::Not),
        token => Err(format!("expect_operator: fail! token = {:?}", token)),
    }
}


fn expect_increment_op(stream: &mut Parser) -> ParseResult<IncrementOp> {
    match stream.try_next()? {
        Tag::Increment => Ok(IncrementOp::Increment),
        Tag::Decrement => Ok(IncrementOp::Decrement),
        token => Err(format!("expect_operator: fail! token = {:?}", token)),
    }
}

fn expect_assign_op(stream: &mut Parser) -> ParseResult<AssignmentOp> {
    match stream.try_next()? {
        Tag::Assign => Ok(AssignmentOp::Assign),
        Tag::PlusAssign => Ok(AssignmentOp::Add),
        Tag::MinusAssign => Ok(AssignmentOp::Subtract),
        Tag::SlashAssign => Ok(AssignmentOp::Divide),
        Tag::AsteriskAssign => Ok(AssignmentOp::Multiply),
        Tag::PercentAssign => Ok(AssignmentOp::Remainder),
        Tag::AmpersandAssign => Ok(AssignmentOp::BitAnd),
        Tag::CaretAssign => Ok(AssignmentOp::BitXor),
        Tag::PipeAssign => Ok(AssignmentOp::BitOr),
        Tag::LeftShiftAssign => Ok(AssignmentOp::LeftShift),
        Tag::RightShiftAssign => Ok(AssignmentOp::RightShift),
        token => Err(format!("expect_operator: fail! token = {:?}", token)),
    }
}


fn parse_binary_expr(stream: &mut Parser, left: Expr, precedence: u8) -> ParseResult<BinaryExpr> {
    //    println!("parse binary expr: {}", stream.position);
    let op = expect_binary_op(stream)?;
    let right = parse_expr_with_precedence(stream, precedence)?;
    let expr = BinaryExpr::new(op, left, right);
    Ok(expr)
}

fn parse_name_expr(stream: &mut Parser) -> ParseResult<Expr> {
    let name = name(stream)?;
    Ok(Expr::Name(name))
}

fn parse_expr_or_type(stream: &mut Parser) -> ParseResult<ExprOrType> {
    stream.mark();
    match parse_type(stream) {
        Ok(ty) => Ok(ExprOrType::Type(ty)),
        Err(_why) => {
            stream.rewind();
            let expr = parse_expr(stream)?;

            Ok(ExprOrType::Expr(Box::new(expr)))
        }
    }
}

fn parse_cast_or_paren_expr(stream: &mut Parser) -> ParseResult<Expr> {
    stream.consume(&Tag::LeftParen)?;
    let expr_or_type = parse_expr_or_type(stream)?;
    stream.consume(&Tag::RightParen)?;

    let expr = match expr_or_type {
        ExprOrType::Type(ty) => {
            let expr = parse_expr(stream)?;
            let expr = CastExpr::new(ty, expr);
            Expr::Cast(expr)
        },
        ExprOrType::Expr(expr) => *expr,
    };

    Ok(expr)
}


fn parse_assign_expr(stream: &mut Parser, left: Expr, precedence: u8) -> ParseResult<AssignmentExpr> {
    let op = expect_assign_op(stream)?;
    let right = parse_expr_with_precedence(stream, precedence - 1)?; // -1 to make it right associtivy
    let expr = AssignmentExpr::new(op, left, right);
    Ok(expr)
}

fn parse_sizeof_expr(stream: &mut Parser) -> ParseResult<Expr> {
    stream.consume(&Tag::Sizeof)?;

    let value = if stream.accept(&Tag::LeftParen) {
        let expr_or_type = parse_expr_or_type(stream)?;
        stream.consume(&Tag::RightParen)?;
        expr_or_type
    } else {
        let expr = parse_expr(stream)?;
        ExprOrType::Expr(Box::new(expr))
    };

    let expr = SizeOfExpr::new(value);
    let expr = Expr::SizeOf(expr);
    Ok(expr)
}

fn parse_prefix_increment_expr(stream: &mut Parser) -> ParseResult<Expr> {
    let op = expect_increment_op(stream)?;
    let expr = parse_expr(stream)?;
    let expr = IncrementExpr::new(op, IncrementMode::Prefix, expr);
    let expr = Expr::Increment(expr);
    Ok(expr)
}

fn parse_postfix_increment_expr(stream: &mut Parser, left: Expr, _precedence: u8) -> ParseResult<IncrementExpr> {
    let op = expect_increment_op(stream)?;
    let expr = IncrementExpr::new(op, IncrementMode::Postfix, left);
    Ok(expr)
}

fn parse_array_access_expr(stream: &mut Parser, left: Expr, precedence: u8) -> ParseResult<ArrayAccessExpr> {
    stream.consume(&Tag::LeftBracket)?;
    let right = parse_expr_with_precedence(stream, precedence)?;
    let expr = ArrayAccessExpr::new(left, right);
    stream.consume(&Tag::RightBracket)?;
    Ok(expr)
}

fn parse_access_expr(stream: &mut Parser, left: Expr, _precedence: u8) -> ParseResult<AccessExpr> {
    let op = expect_access_op(stream)?;
    let right = name(stream)?;
    let expr = AccessExpr::new(op, left, right);
    Ok(expr)
}

fn parse_logical_expr(stream: &mut Parser, left: Expr, precedence: u8) -> ParseResult<LogicalExpr> {
    let op = expect_logical_op(stream)?;
    let right = parse_expr_with_precedence(stream, precedence)?;
    let expr = LogicalExpr::new(op, left, right);
    Ok(expr)
}

fn parse_bitwise_expr(stream: &mut Parser, left: Expr, precedence: u8) -> ParseResult<BitwiseExpr> {
    let op = expect_bitwise_op(stream)?;
    let right = parse_expr_with_precedence(stream, precedence)?;
    let expr = BitwiseExpr::new(op, left, right);
    Ok(expr)
}

fn parse_cmp_expr(stream: &mut Parser, left: Expr, precedence: u8) -> ParseResult<CmpExpr> {
    let op = expect_cmp_op(stream)?;
    let right = parse_expr_with_precedence(stream, precedence)?;
    let expr = CmpExpr::new(op, left, right);
    Ok(expr)
}

fn parse_elvis_expr(stream: &mut Parser, expr: Expr, _: u8) -> ParseResult<ElvisExpr> {
    stream.consume(&Tag::Question)?;
    let then_expr = parse_expr(stream)?;
    stream.consume(&Tag::Colon)?;
    let else_expr = parse_expr(stream)?;
    let expr = ElvisExpr::new(expr, then_expr, else_expr);
    Ok(expr)
}

fn parse_comma_expr(stream: &mut Parser, left: Expr, precedence: u8) -> ParseResult<CommaExpr> {
    stream.consume(&Tag::Comma)?;
    let right = parse_expr_with_precedence(stream, precedence)?;
    let expr = CommaExpr::new(left, right);
    Ok(expr)
}

fn parse_unary_expr(stream: &mut Parser) -> ParseResult<Expr> {
    let op = expect_unary_op(stream)?;
    let expr = parse_expr(stream)?;
    let expr = UnaryExpr::new(op, expr);
    let expr = Expr::Unary(expr);
    Ok(expr)
}

fn parse_decl_stmt(stream: &mut Parser) -> ParseResult<DeclStmt> {
    let typ = parse_type(stream)?;
    let name = name(stream)?;
    let expr = optional(stream, &Tag::Assign, parse_expr)?;
    stream.consume(&Tag::Semicolon)?;

    let stmt = DeclStmt(typ, name, expr);
    Ok(stmt)
}

fn parse_type(stream: &mut Parser) -> ParseResult<Type> {
    //    let token = ;
    // FIXME: parse static/const too
    let typ = match stream.try_next()? {
        Tag::Char => Type::Primitive(PrimitiveType::Char),
        Tag::Short => Type::Primitive(PrimitiveType::Short),
        Tag::Int => Type::Primitive(PrimitiveType::Int),
        Tag::Long => Type::Primitive(PrimitiveType::Long),
        Tag::Float => Type::Primitive(PrimitiveType::Float),
        Tag::Double => Type::Primitive(PrimitiveType::Double),
        Tag::Void => Type::Primitive(PrimitiveType::Void),
        Tag::Unsigned => match stream.try_next()? {
            Tag::Char => Type::Primitive(PrimitiveType::UChar),
            Tag::Short => Type::Primitive(PrimitiveType::UShort),
            Tag::Int => Type::Primitive(PrimitiveType::UInt),
            Tag::Long => Type::Primitive(PrimitiveType::ULong),
            _ => panic!("Unhandled token in parse_type after unsigned"),
        },
        Tag::Identifier(name) => {
            Type::Name(name)
        },
        t => return Err(format!("Unhandled token in parse_type: {:?}", t)),
    };

    Ok(typ)
}


fn parse_call_expr(stream: &mut Parser, left: Expr, precedence: u8) -> ParseResult<CallExpr> {
//    println!("masuk sini");
    let args = group_with_delimiter(
        stream,
        &Tag::LeftParen,
        &Tag::RightParen,
        &Tag::Comma,
        |stream| parse_expr_with_precedence(stream, precedence)
    )?;
    let expr = CallExpr::new(left, args);
    Ok(expr)
}


fn parse_number_literal(stream: &mut Parser) -> ParseResult<Expr> {
//    println!("parse number literal: {}", stream.position);
    match stream.try_next()? {
        Tag::Number(number) => match number {
            NumberToken::Integer(n, _) => {
                let value: usize = n.parse().unwrap();
                let expr = Expr::Integer(IntegerLit(value));
                Ok(expr)
            },
            NumberToken::Float(n, _) => {
                // FIXME: actually this is wrong because C float literal is not compatible with rust one
                let value: f64 = n.parse().unwrap();
                let expr = Expr::Float(FloatLit(value));
                Ok(expr)
            }
        },
        t => Err(format!("unexpected token: {:?}", t))
    }
}

fn parse_string_literal(stream: &mut Parser) -> ParseResult<Expr> {
    match stream.try_next()? {
        Tag::String(value) => Ok(Expr::String(StringLit(value))),
        t => Err(format!("unexpected token: {:?}", t))
    }
}

fn parse_char_literal(stream: &mut Parser) -> ParseResult<Expr> {
    match stream.try_next()? {
        Tag::Character(value) => Ok(Expr::Char(CharLit(value))),
        t => Err(format!("unexpected token: {:?}", t))
    }
}



fn parse_union_spec(stream: &mut Parser) -> ParseResult<UnionSpec> {
    stream.consume(&Tag::Union)?;
    let name = name(stream)?;
    let members = group(stream, &Tag::LeftBrace, &Tag::RightBrace, parse_var_decl)?;

    Ok(UnionSpec(name, members))
}

fn parse_struct_spec(stream: &mut Parser) -> ParseResult<StructSpec> {
    stream.consume(&Tag::Struct)?;
    let name = name(stream)?;
    let members = group(stream, &Tag::LeftBrace, &Tag::RightBrace, parse_var_decl)?;

    Ok(StructSpec(name, members))
}



fn parse_var_decl(stream: &mut Parser) -> ParseResult<VarDecl> {
    let ty = parse_type(stream)?;
    let name = name(stream)?;
    stream.consume(&Tag::Semicolon)?;

    Ok(VarDecl(ty, name))
}

fn parse_function_decl(stream: &mut Parser) -> ParseResult<FunctionDecl> {
    let ty = parse_type(stream)?;
    let name = name(stream)?;
    let params = group_with_delimiter(
        stream, &Tag::LeftParen, &Tag::RightParen, &Tag::Comma, parse_param_decl)?;

    Ok(FunctionDecl(ty, name, params))
}

fn parse_param_decl(stream: &mut Parser) -> ParseResult<ParamDecl> {
    let ty = parse_type(stream)?;
    let name = name(stream)?;

    Ok(ParamDecl(ty, name))
}


fn parse_enum_spec(stream: &mut Parser) -> ParseResult<EnumSpec> {
    stream.consume(&Tag::Enum)?;
    let name = name(stream)?;
    let members = group_with_delimiter(stream,
                                       &Tag::LeftBrace,
                                       &Tag::RightBrace,
                                       &Tag::Comma,
                                       parse_enum_member_spec
    )?;

    Ok(EnumSpec(name, members))
}

fn parse_enum_member_spec(stream: &mut Parser) -> ParseResult<EnumMemberSpec> {
    let name = name(stream)?;
    let init_expr = optional(stream, &Tag::Assign, parse_expr)?;
    Ok(EnumMemberSpec(name, init_expr))
}

fn parse_typedef_spec(stream: &mut Parser) -> ParseResult<TypeDefDecl> {
    stream.consume(&Tag::Typedef)?;
    let typ_spec = match stream.try_peek()? {
        Tag::Enum => TypeSpec::Enum(parse_enum_spec(stream)?),
        Tag::Struct => TypeSpec::Struct(parse_struct_spec(stream)?),
        Tag::Union => TypeSpec::Union(parse_union_spec(stream)?),
        ref token if token.is_type() => TypeSpec::Type(parse_type(stream)?),
        _ => return Err(format!("cannot parse typedef spec")),
    };
    let name = name(stream)?;
    Ok(TypeDefDecl(typ_spec, name))
}


fn parse_decl(stream: &mut Parser) -> ParseResult<Decl> {
    let decl = match stream.try_peek()? {
        Tag::Enum => Decl::Enum(parse_enum_spec(stream)?),
        Tag::Struct => Decl::Struct(parse_struct_spec(stream)?),
        Tag::Union => Decl::Union(parse_union_spec(stream)?),
        Tag::Typedef => Decl::TypeDef(parse_typedef_spec(stream)?),
        _ => Decl::Function(parse_function_decl(stream)?),
    };

    match decl {
        Decl::Function(function_decl) => {
            if stream.try_peek()? == Tag::LeftBrace {
                let function_def = parse_function_def(stream, function_decl)?;
                let decl = Decl::FunctionDef(function_def);
                return Ok(decl);
            }else{
                stream.consume(&Tag::Semicolon)?;
                return Ok(Decl::Function(function_decl));
            }
        },
        _ => {
            stream.consume(&Tag::Semicolon)?;
            return Ok(decl);
        },
    }
}

fn parse_function_def(stream: &mut Parser, function_decl: FunctionDecl) -> ParseResult<FunctionDef> {
    let body = group(stream, &Tag::LeftBrace, &Tag::RightBrace, parse_stmt)?;
    Ok(FunctionDef(function_decl, body))
}


#[cfg(test)]
mod testing {
    use super::*;
    use crate::lexer;

    /// macro for testing token
    macro_rules! assert_token {
        ($lexer:expr, $token:expr) => {{
            assert_eq!($lexer, Some($token));
        }};
    }

    /// macro for testing token number
    macro_rules! assert_token_number {
        ($lexer:expr, $token:expr, $value:expr, $suffix:expr) => {{
            assert_eq!($lexer, Some(Token::Number(
                $token($value.to_string(), $suffix)
            )));
        }};
    }

    macro_rules! assert_match {
        ($expr:expr, $pat: pat) => {{
            #![allow(unreachable_patterns)]
            match $expr {
                $pat => {},
                _ => { panic!("cannot matching")}
            }
        }};
    }


    macro_rules! prepare_stream {
        ($input: expr) => {{
            let input = $input;
            let result = lexer::tokenize(input).unwrap();
            let tokens = result.tokens;
            Parser::new(tokens)
        }};
    }

    /// helper function to prepare expr 
    fn prepare_expr(text: &str) -> Expr {
        let mut stream = prepare_stream!(text);
        let result = parse_expr(&mut stream).unwrap();
        result
    }

    /// helper function to prepare type 
    fn prepare_type(input: &str) -> Type {
        let mut stream = prepare_stream!(input);
        let result = parse_type(&mut stream).unwrap();
        result
    }


    #[test]
    fn test_stream_peek_and_next() {
        let mut stream = prepare_stream!("1 + 2");

        assert_token_number!(stream.peek(), NumberToken::Integer, "1", IntegerSuffix::SUFFIX_NONE);
        assert_token_number!(stream.next(), NumberToken::Integer, "1", IntegerSuffix::SUFFIX_NONE);

        assert_token!(stream.peek(), Tag::Plus);
        assert_token_number!(stream.peek_again(), NumberToken::Integer, "2", IntegerSuffix::SUFFIX_NONE);
        assert_token!(stream.next(), Tag::Plus);

        assert_token_number!(stream.peek(), NumberToken::Integer, "2", IntegerSuffix::SUFFIX_NONE);
        assert_token_number!(stream.next(), NumberToken::Integer, "2", IntegerSuffix::SUFFIX_NONE);
    }

    #[test]
    fn test_stream_expect_binary_op() {
        let mut stream = prepare_stream!("+ - * / NOT_OP");

        assert_eq!(expect_binary_op(&mut stream).unwrap(), BinaryOp::Add);
        assert_eq!(expect_binary_op(&mut stream).unwrap(), BinaryOp::Subtract);
        assert_eq!(expect_binary_op(&mut stream).unwrap(), BinaryOp::Multiply);
        assert_eq!(expect_binary_op(&mut stream).unwrap(), BinaryOp::Divide);
        expect_binary_op(&mut stream).unwrap_err();
    }

    #[test]
    fn test_stream_consume() {
        let mut stream = prepare_stream!("+ - * /");

        stream.consume(&Tag::Plus).unwrap();
        stream.consume(&Tag::Minus).unwrap();
        stream.consume(&Tag::Asterisk).unwrap();
        stream.consume(&Tag::Plus).unwrap_err();
    }

    #[test]
    fn test_paren_expr() {
        let result = prepare_expr("(1)");
        assert_eq!(result, Expr::Integer(IntegerLit(1)));
    }

    #[test]
    fn test_assign_expr() {
        let result = prepare_expr("a = 20 * 10");
        assert_eq!(result, Expr::Assignment(AssignmentExpr {
            op: AssignmentOp::Assign,
            left: Box::new(Expr::Name("a".to_string())),
            right: Box::new(Expr::Binary(BinaryExpr {
                op: BinaryOp::Multiply,
                left: Box::new(Expr::Integer(IntegerLit(20))),
                right: Box::new(Expr::Integer(IntegerLit(10))),
            }))
        }));
    }

    #[test]
    fn test_literal_integer_expr() {
        let result = prepare_expr("a = 20");
        match result {
            Expr::Assignment(expr) => {
                assert_eq!(expr.right, Box::new(Expr::Integer(IntegerLit(20))))
            },
            _ => panic!("unexpected AST"),
        }
    }

    #[test]
    fn test_literal_float_expr() {
        let result = prepare_expr("a = 20.1");
        match result {
            Expr::Assignment(expr) => {
                assert_eq!(expr.right, Box::new(Expr::Float(FloatLit(20.1))))
            },
            _ => panic!("unexpected AST"),
        }
    }

    #[test]
    fn test_literal_char_expr() {
        let result = prepare_expr("a = 'c'");
        match result {
            Expr::Assignment(expr) => {
                assert_eq!(expr.right, Box::new(Expr::Char(CharLit('c'))))
            },
            _ => panic!("unexpected AST"),
        }
    }

    #[test]
    fn test_call_expr() {
        let result = prepare_expr("foo(a,b,10)");
        assert_eq!(result, Expr::Call(CallExpr {
            expr: Box::new(Expr::Name("foo".to_string())),
            args: vec![
                Expr::Name("a".to_string()),
                Expr::Name("b".to_string()),
                Expr::Integer(IntegerLit(10)),
            ]
        }));
    }

    #[test]
    fn test_cast_expr() {
        let result = prepare_expr("(foo) a");
        assert_eq!(result, Expr::Cast(CastExpr(
            Type::Name("foo".to_string()),
            Box::new(Expr::Name("a".to_string()))
        )));
    }

    #[test]
    fn test_sizeof_expr(){
        let result = prepare_expr("sizeof(int)");
        assert_eq!(result, Expr::SizeOf(SizeOfExpr{
            expr_or_type: ExprOrType::Type(Type::Primitive(
                PrimitiveType::Int
            ))
        }));

        let result = prepare_expr("sizeof(10)");
        assert_eq!(result, Expr::SizeOf(SizeOfExpr{
            expr_or_type: ExprOrType::Expr(Box::new(Expr::Integer(
                IntegerLit(10)
            )))
        }));
    }

    #[test]
    fn test_precendence_comma() {
        let result = prepare_expr("10 + 20, 11 * 77, foo(20), 40");
        assert_match!(result, Expr::Comma(CommaExpr{..}));
    }

    #[test]
    fn test_precendence_binary() {
        let result = prepare_expr("1 + 2 * (4 - 1)");
        match result {
            Expr::Binary(expr1) => {
                assert_eq!(expr1.op, BinaryOp::Add);
                match expr1.right.as_ref() {
                    &Expr::Binary(ref expr2) => {
                        assert_eq!(expr2.op, BinaryOp::Multiply);
                        match expr2.right.as_ref() {
                            &Expr::Binary(ref expr3) => {
                                assert_eq!(expr3.op, BinaryOp::Subtract);
                            }
                            _ => panic!("Unexpected AST")
                        }
                    }
                    _ => panic!("Unexpected AST")
                }
            }
            _ => panic!("Unexpected AST")
        }
    }

    #[test]
    fn test_expr_right_associaty() {
        let result = prepare_expr("a = b = c");
        match result {
            Expr::Assignment(expr) => {
                assert_eq!(expr.op, AssignmentOp::Assign);
                assert_eq!(expr.left, Box::new(Expr::Name("a".to_string())));
                assert_match!(expr.right.as_ref(), &Expr::Assignment(..));
            },
            _ => panic!("unexpected AST")
        }
    }

    #[test]
    fn test_expr_stmt() {
        let mut stream = prepare_stream!("1 + 2 * (4 - 1);");
        let result = parse_stmt(&mut stream).unwrap();
        assert_match!(result, Stmt::Expr(_));
    }

    #[test]
    fn test_return_stmt() {
        let mut stream = prepare_stream!("return 20 * 40;");
        let result = parse_stmt(&mut stream).unwrap();
        assert_match!(result, Stmt::Return(ReturnStmt(Some(_))));
    }

    #[test]
    fn test_empty_return_stmt(){
        let mut stream = prepare_stream!("return;");
        let result = parse_stmt(&mut stream).unwrap();
        assert_match!(result, Stmt::Return(ReturnStmt(None)));
    }

    #[test]
    fn test_decl_stmt() {
        let mut stream = prepare_stream!("int foo = 2;");
        let result = parse_stmt(&mut stream).unwrap();
        match result {
            Stmt::Decl(DeclStmt(..)) => {},
            _ => panic!("Unexpected Statement")
        }
    }

    #[test]
    fn test_type() {
        let int_type = prepare_type("int");
        let uint_type = prepare_type("unsigned short");
        let id_type = prepare_type("foo_bar");

        assert_eq!(int_type, Type::Primitive(PrimitiveType::Int));
        assert_eq!(uint_type, Type::Primitive(PrimitiveType::UShort));
        assert_eq!(id_type, Type::Name("foo_bar".to_string()));
    }


    #[test]
    fn test_struct_decl_1() {
        let mut stream = prepare_stream!("struct foo{int a; int b;};");
        let result = parse_decl(&mut stream).unwrap();
        match result {
            Decl::Struct(result) => {
                assert_match!(result, StructSpec(_, _));
                assert_eq!(result.0, "foo".to_string());
                assert_eq!(result.1[0], VarDecl(
                    Type::Primitive(PrimitiveType::Int),
                    "a".to_string()
                ));
            }
            _ => panic!("not struct")
        }
    }

    #[test]
    fn test_enum_decl_1() {
        let mut stream = prepare_stream!("enum foo{a, b, c};");
        let result = parse_decl(&mut stream).unwrap();
        match result {
            Decl::Enum(result) => {
                assert_match!(result, EnumSpec(..));
                assert_eq!(result.0, "foo".to_string());
                assert_eq!(result.1[0], EnumMemberSpec("a".to_string(), None));
                assert_eq!(result.1[1], EnumMemberSpec("b".to_string(), None));
                assert_eq!(result.1[2], EnumMemberSpec("c".to_string(), None));
            },
            _ => panic!("not enum")
        }
    }

    #[test]
    fn test_typedef_decl() {
        let mut stream = prepare_stream!("typedef union foo{int a; short b; char c;} FOO;");
        let result = parse_decl(&mut stream).unwrap();
        match result {
            Decl::TypeDef(TypeDefDecl(TypeSpec::Union(union_), name)) => {
                assert_eq!(name, "FOO".to_string());
                assert_match!(union_, UnionSpec(..));
            },
            _ => panic!("not enum")
        }
    }


    #[test]
    fn test_function_def_empty_body() {
        let mut stream = prepare_stream!(r##"void main(){}"##);
        let result = parse_decl(&mut stream).unwrap();
        match result {
            Decl::FunctionDef(FunctionDef(FunctionDecl(typ, name, _params), ..)) => {
                assert_eq!(typ, Type::Primitive(PrimitiveType::Void));
                assert_eq!(name, "main".to_string());
            },
            _ => panic!("unexpected AST")
        }
    }

    #[test]
    fn test_function_def_single_body_item() {
        let mut stream = prepare_stream!(r##"void main(){printf("hello world");}"##);
        let result = parse_decl(&mut stream).unwrap();
        match result {
            Decl::FunctionDef(FunctionDef(FunctionDecl(typ, name, _params), body)) => {
                assert_eq!(typ, Type::Primitive(PrimitiveType::Void));
                assert_eq!(name, "main".to_string());
                assert_eq!(body.len(), 1);
            },
            _ => panic!("unexpected AST")
        }
    }

    #[test]
    fn parse_tu_simple(){
        let input = include_str!("../test-cases/00-simple-translation-unit.c");
        let lexer_result = lexer::tokenize(input).unwrap();
        let tu = parse(lexer_result.tokens, lexer_result.bookmark).unwrap();

        assert_eq!(tu.decls.len(), 1);
    }

}
