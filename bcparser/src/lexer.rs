use crate::bookmark::*;
use crate::token::*;

use std::char;
use std::str;

pub struct Lexer<'a> {
    input: &'a [u8],

    // lexing state
    position: usize,

    // bookmark to marking position of line and column
    bookmark: Bookmark,
}

pub type LexingError = ();

pub struct LexingResult {
    pub tokens: TokenVec,
    pub bookmark: Bookmark,
}

macro_rules! match_peek_char {
    ($lexer:ident, $default:expr, {
        $( $ch:pat => $token:expr ),*
    }) => {
        match $lexer.peek_char() {
            $( Some(&$ch) => {
                $lexer.skip_char(1);
                $token
            })*
            _ => $default
        }
    };
}

/// Tokenize the input string and return the tokens. Last Token will be always have Tag::EndOfLine
pub fn tokenize(input: &str) -> Result<LexingResult, LexingError> {
    let mut lexer = Lexer::new(&input);
    let mut tokens = TokenVec::new();

    while lexer.is_eof() == false {
        let (token, _) = lexer.next_token();
        match token.tag {
            Tag::LineComment | Tag::BlockComment => {}
            Tag::EndOfLine => {
                // skip
            }
            _ => {
                tokens.push(token);
            }
        }
    }

    let bookmark = lexer.bookmark;
    let result = LexingResult { tokens, bookmark };

    Ok(result)
}

/// is_letter checks whether a char is a valid alphabetic character or an underscore
fn is_letter(ch: u8) -> bool {
    ch.is_ascii_alphabetic() || ch == b'_'
}

fn is_whitespace(ch: u8) -> bool {
    const VT: u8 = 11;
    const FF: u8 = 12;

    if ch != b' ' && ch != b'\t' && ch != b'\r' && ch != VT && ch != FF {
        false
    } else {
        true
    }
}

impl<'a> Lexer<'a> {
    pub fn new(input: &'a str) -> Lexer<'a> {
        Lexer {
            input: input.as_bytes(),
            position: 0,
            bookmark: Bookmark::new(),
        }
    }

    fn is_eof(&self) -> bool {
        self.position > self.input.len()
    }

    fn new_line(&mut self) -> Tag {
        self.bookmark.mark(self.position as Offset);
        Tag::EndOfLine
    }

    fn read_char(&mut self) -> Option<&u8> {
        let ch = self.input.get(self.position);
        self.position += 1;
        ch
    }

    fn peek_char(&mut self) -> Option<&u8> {
        let ch = self.input.get(self.position);
        ch
    }

    fn peek_char_eq(&mut self, ch: u8) -> bool {
        match self.peek_char() {
            Some(&peek_ch) => peek_ch == ch,
            None => false,
        }
    }

    fn peek_char_eq_many(&mut self, chars: &[u8]) -> bool {
        for &ch in chars {
            match self.peek_char() {
                Some(&peek_ch) if peek_ch == ch => return true,
                None => return false,
                _ => continue,
            }
        }
        false
    }

    fn peek_char_eq_lowercase(&mut self, ch: char) -> bool {
        let ch1 = ch as u8;
        let ch2 = ch.to_ascii_lowercase() as u8;

        match self.peek_char() {
            Some(&peek_ch) => peek_ch == ch1 || peek_ch == ch2,
            None => false,
        }
    }

    fn skip_char(&mut self, n: usize) {
        self.position += n;
    }

    /// skip whitespace
    fn skip_whitespace(&mut self) {
        loop {
            if self.peek_is(is_whitespace) {
                self.skip_char(1);
            } else {
                break;
            }
        }
    }

    /// test next character with spesific function
    fn peek_is<F: Fn(u8) -> bool>(&mut self, tester: F) -> bool {
        match self.peek_char() {
            Some(&ch) => tester(ch),
            None => false,
        }
    }

    /// skip until newline found
    fn skip_until_newline(&mut self) {
        loop {
            match self.peek_char() {
                Some(b'\n') | None => break,
                _ => self.skip_char(1),
            }
        }
    }

    fn parse_identifier_or_keyword(&mut self, begin: usize) -> Tag {
        let mut end = begin + 1;
        while self.peek_is(is_letter) {
            self.read_char();
            end += 1;
        }

        let slice = &self.input[begin..end];
        match slice {
            b"auto" => Tag::Auto,
            b"break" => Tag::Break,
            b"case" => Tag::Case,
            b"char" => Tag::Char,
            b"const" => Tag::Const,
            b"continue" => Tag::Continue,
            b"default" => Tag::Default,
            b"do" => Tag::Do,
            b"double" => Tag::Double,
            b"else" => Tag::Else,
            b"enum" => Tag::Enum,
            b"extern" => Tag::Extern,
            b"float" => Tag::Float,
            b"false" => Tag::False,
            b"for" => Tag::For,
            b"goto" => Tag::Goto,
            b"if" => Tag::If,
            b"int" => Tag::Int,
            b"long" => Tag::Long,
            b"register" => Tag::Register,
            b"return" => Tag::Return,
            b"short" => Tag::Short,
            b"signed" => Tag::Signed,
            b"sizeof" => Tag::Sizeof,
            b"static" => Tag::Static,
            b"struct" => Tag::Struct,
            b"switch" => Tag::Switch,
            b"true" => Tag::True,
            b"typedef" => Tag::Typedef,
            b"union" => Tag::Union,
            b"unsigned" => Tag::Unsigned,
            b"void" => Tag::Void,
            b"volatile" => Tag::Volatile,
            b"while" => Tag::While,
            _ => Tag::Identifier,
        }
    }

    fn skip_number_integer(&mut self, begin: usize) -> IntegerSuffix {
        // let mut end = begin + 1;

        // check radix
        let radix = match self.input[begin] {
            b'0' => {
                if self.peek_char_eq_lowercase('X') {
                    self.skip_char(1);
                    16
                } else {
                    8
                }
            }
            _ => 10,
        };
        self.read_digit(radix);

        // get suffix
        let mut suffix = IntegerSuffix::SUFFIX_NONE;
        if self.peek_char_eq_lowercase('U') {
            suffix = IntegerSuffix::SUFFIX_U;
            self.read_char();
        }

        if self.peek_char_eq_lowercase('L') {
            suffix |= IntegerSuffix::SUFFIX_L;
            self.read_char();

            if self.peek_char_eq_lowercase('L') {
                suffix.remove(IntegerSuffix::SUFFIX_L);
                suffix |= IntegerSuffix::SUFFIX_LL;
                self.read_char();
            }

            if suffix.contains(IntegerSuffix::SUFFIX_U) == false && self.peek_char_eq_lowercase('U')
            {
                suffix |= IntegerSuffix::SUFFIX_U;
                self.read_char();
            }
        }

        suffix
    }

    fn skip_number_float(&mut self, _begin: usize) -> FloatSuffix {
        loop {
            match self.peek_char() {
                None => break,
                Some(&b'.') => {
                    self.read_char();
                    self.read_digit(10);
                }
                Some(&b'e') | Some(&b'E') => {
                    self.read_char();
                    self.read_digit(10);
                    break;
                }
                Some(_) => break,
            }
        }

        // float suffix
        let mut suffix = FloatSuffix::None;
        if self.peek_char_eq_lowercase('F') {
            suffix = FloatSuffix::Float;
            self.read_char();
        } else if self.peek_char_eq_lowercase('L') {
            suffix = FloatSuffix::Long;
            self.read_char();
        }

        suffix
    }

    /// advance lexer position until the next character is not digit
    /// with spesific radix
    fn read_digit(&mut self, radix: u32) -> usize {
        let mut end = 0;
        while let Some(&c) = self.peek_char() {
            if radix == 10 && c.is_ascii_digit() == false {
                break;
            } else if radix == 8 && c.is_ascii_digit() == false {
                break;
            } else if radix == 16 && c.is_ascii_hexdigit() == false {
                break;
            }
            self.skip_char(1);
            end += 1;
        }
        end
    }

    /// parse C integer/Float format
    fn parse_number(&mut self, begin: usize) -> Tag {
        let suffix = self.skip_number_integer(begin);
        if suffix == IntegerSuffix::SUFFIX_NONE && self.peek_char_eq_many(&[b'.', b'e', b'E']) {
            self.skip_number_float(begin);
            return Tag::LiteralFloat;
        }

        Tag::LiteralInteger
    }

    fn parse_char(&mut self) -> Tag {
        match self.read_char() {
            None => {
                println!("Illegal End of char");
                Tag::Illegal
            }
            Some(_) => {
                match_peek_char!(self, Tag::Illegal, {
                    b'\'' => Tag::Character
                })
            }
        }
    }

    fn parse_string(&mut self) -> Tag {
        loop {
            match self.read_char() {
                None => {
                    println!("Illegal End of string");
                    return Tag::Illegal;
                }
                Some(&b'"') => {
                    return Tag::String;
                }
                Some(_) => {}
            }
        }
    }

    /// return token and last position
    pub fn next_token(&mut self) -> (Token, Offset) {
        self.skip_whitespace();

        let current_pos = self.position;
        let tag = match self.read_char() {
            None => {
                self.new_line();
                Tag::EndOfFile
            }
            Some(&ch) => match ch {
                b'$' => Tag::Dollar,
                b'?' => Tag::Question,
                b':' => match_peek_char!(self, Tag::Colon, {
                    b':' => Tag::DoubleColon
                }),
                b';' => Tag::Semicolon,
                b',' => Tag::Comma,
                b'~' => Tag::Tilde,
                b'.' => Tag::Dot,
                b'=' => match_peek_char!(self, Tag::Assign,{
                   b'=' => Tag::Equal
                }),
                b'+' => match_peek_char!(self, Tag::Plus, {
                    b'+' => Tag::Increment,
                    b'=' => Tag::PlusAssign
                }),
                b'-' => match_peek_char!(self, Tag::Minus, {
                    b'-' => Tag::Decrement,
                    b'=' => Tag::MinusAssign,
                    b'>' => Tag::Arrow
                }),
                b'!' => match_peek_char!(self, Tag::Bang, {
                    b'=' => Tag::NotEqual
                }),
                b'&' => match_peek_char!(self, Tag::Ampersand, {
                    b'&' => Tag::LogicalAnd,
                    b'=' => Tag::AmpersandAssign
                }),
                b'|' => match_peek_char!(self, Tag::Pipe, {
                    b'|' => Tag::LogicalOr,
                    b'=' => Tag::PipeAssign
                }),
                b'^' => match_peek_char!(self, Tag::Caret, {
                    b'=' => Tag::CaretAssign
                }),
                b'%' => match_peek_char!(self, Tag::Percent, {
                    b'=' => Tag::PercentAssign
                }),
                b'/' => match_peek_char!(self, Tag::Slash, {
                    b'=' => Tag::SlashAssign,
                    b'/' => {
                        self.skip_until_newline();
                        Tag::LineComment
                    },
                    b'*' => {
                        loop {
                            match self.read_char(){
                                None => break,
                                Some(&b'\n') => {
                                    self.new_line();
                                },
                                Some(&b'*') => {
                                    if self.peek_char_eq(b'/'){
                                        self.skip_char(1);
                                        break
                                    }
                                }
                                Some(_) => {},
                            }
                        }
                        Tag::BlockComment
                    }
                }),
                b'*' => match_peek_char!(self, Tag::Asterisk,{
                    b'=' => Tag::AsteriskAssign
                }),
                b'<' => match_peek_char!(self, Tag::LowerThan, {
                    b'<' => Tag::LeftShift,
                    b'=' => Tag::LowerThanEqual
                }),
                b'>' => match_peek_char!(self, Tag::GreaterThan, {
                    b'>' => Tag::RightShift,
                    b'=' => Tag::GreaterThanEqual
                }),

                b'{' => Tag::LeftBrace,
                b'}' => Tag::RightBrace,
                b'(' => Tag::LeftParen,
                b')' => Tag::RightParen,
                b'[' => Tag::LeftBracket,
                b']' => Tag::RightBracket,

                b'0'..=b'9' => self.parse_number(current_pos),
                b'_' | b'a'..=b'z' | b'A'..=b'Z' => self.parse_identifier_or_keyword(current_pos),

                b'\'' => self.parse_char(),
                b'"' => self.parse_string(),

                b'\r' => match_peek_char!(self, Tag::Illegal, {
                    b'`' => self.new_line()
                }),
                b'\n' => self.new_line(),
                illegal_ch => {
                    println!("illegal char: {}", illegal_ch);
                    Tag::Illegal // TODO: Maybe we need ch here, to display a nice error message later?
                }
            },
        };

        let token = Token::new(tag, current_pos as Offset);
        let end_position = self.position as Offset;
        (token, end_position)
    }
}

#[cfg(test)]
mod testing {
    /// macro for testing token
    macro_rules! assert_tag {
        ($lexer:expr, $token:expr) => {{
            assert_eq!($lexer.next_token().0.tag, $token);
        }};
    }

    macro_rules! assert_token_lexeme {
        ($lexer:expr, $token:expr, $value:expr) => {{
            let (token, end_position) = $lexer.next_token();
            assert_eq!(token.tag, $token);

            let end_position = end_position.min($lexer.input.len() as u32);
            let lexeme =
                str::from_utf8(&$lexer.input[token.start as usize..end_position as usize]).unwrap();
            assert_eq!(lexeme, $value);
        }};
    }

    macro_rules! assert_token_location {
        ($lexer:expr, $begin:expr, $end:expr) => {{
            let (token, end_position) = $lexer.next_token();

            assert_eq!(token.start, $begin);
            assert_eq!(end_position, $end);
        }};
    }

    use super::*;

    #[test]
    fn is_letter_test() {
        assert!(is_letter(b'_'));
        assert!(is_letter(b'a'));
        assert!(is_letter(b'Z'));

        assert!(!is_letter(b'*'));
        assert!(!is_letter(b'1'));
    }

    #[test]
    fn test_only_whitespace() {
        let input = " \t\r";
        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_location() {
        let input = "0123456 +";
        let mut lexer = Lexer::new(input);
        assert_token_location!(lexer, 0, 7); // integer literal
        assert_token_location!(lexer, 8, 9); // plus
        assert_token_location!(lexer, 9, 10); // end-of-file
    }

    #[test]
    fn test_token_plus() {
        let input = "+ ++ += ";
        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::Plus);
        assert_tag!(lexer, Tag::Increment);
        assert_tag!(lexer, Tag::PlusAssign);
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_minus() {
        let input = " - -- -= -> ";
        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::Minus);
        assert_tag!(lexer, Tag::Decrement);
        assert_tag!(lexer, Tag::MinusAssign);
        assert_tag!(lexer, Tag::Arrow);
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_ampersand() {
        let input = " & && &= ";
        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::Ampersand);
        assert_tag!(lexer, Tag::LogicalAnd);
        assert_tag!(lexer, Tag::AmpersandAssign);
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_pipe() {
        let input = " | || |= ";
        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::Pipe);
        assert_tag!(lexer, Tag::LogicalOr);
        assert_tag!(lexer, Tag::PipeAssign);
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_caret() {
        let input = " ^ ^=";
        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::Caret);
        assert_tag!(lexer, Tag::CaretAssign);
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_asterisk() {
        let input = " * *=";
        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::Asterisk);
        assert_tag!(lexer, Tag::AsteriskAssign);
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_percent() {
        let input = " % %=";
        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::Percent);
        assert_tag!(lexer, Tag::PercentAssign);
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_slash() {
        let input = " / /=";
        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::Slash);
        assert_tag!(lexer, Tag::SlashAssign);
    }

    #[test]
    fn test_token_comparison() {
        let input = " == != < <= > >=";
        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::Equal);
        assert_tag!(lexer, Tag::NotEqual);
        assert_tag!(lexer, Tag::LowerThan);
        assert_tag!(lexer, Tag::LowerThanEqual);
        assert_tag!(lexer, Tag::GreaterThan);
        assert_tag!(lexer, Tag::GreaterThanEqual);
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_shift() {
        let input = " << >>";
        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::LeftShift);
        assert_tag!(lexer, Tag::RightShift);
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_simple() {
        let input = " (){}[],?~;.:: :";
        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::LeftParen);
        assert_tag!(lexer, Tag::RightParen);
        assert_tag!(lexer, Tag::LeftBrace);
        assert_tag!(lexer, Tag::RightBrace);
        assert_tag!(lexer, Tag::LeftBracket);
        assert_tag!(lexer, Tag::RightBracket);
        assert_tag!(lexer, Tag::Comma);
        assert_tag!(lexer, Tag::Question);
        assert_tag!(lexer, Tag::Tilde);
        assert_tag!(lexer, Tag::Semicolon);
        assert_tag!(lexer, Tag::Dot);
        assert_tag!(lexer, Tag::DoubleColon);
        assert_tag!(lexer, Tag::Colon);

        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_identifier() {
        let input = " FOO BAR foo bAr";
        let mut lexer = Lexer::new(input);
        assert_token_lexeme!(lexer, Tag::Identifier, "FOO");
        assert_token_lexeme!(lexer, Tag::Identifier, "BAR");
        assert_token_lexeme!(lexer, Tag::Identifier, "foo");
        assert_token_lexeme!(lexer, Tag::Identifier, "bAr");
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_number_integer() {
        let input = "123456789 99U 11u 22l 33L 44uL 55ULL 0xDEADC0DE 0775";
        let mut lexer = Lexer::new(input);

        assert_token_lexeme!(lexer, Tag::LiteralInteger, "123456789");
        assert_token_lexeme!(lexer, Tag::LiteralInteger, "99U");
        assert_token_lexeme!(lexer, Tag::LiteralInteger, "11u");
        assert_token_lexeme!(lexer, Tag::LiteralInteger, "22l");
        assert_token_lexeme!(lexer, Tag::LiteralInteger, "33L");
        assert_token_lexeme!(lexer, Tag::LiteralInteger, "44uL");
        assert_token_lexeme!(lexer, Tag::LiteralInteger, "55ULL");
        assert_token_lexeme!(lexer, Tag::LiteralInteger, "0xDEADC0DE");
        assert_token_lexeme!(lexer, Tag::LiteralInteger, "0775");
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_expression() {
        let input = "1 + 2";
        let mut lexer = Lexer::new(input);

        assert_token_lexeme!(lexer, Tag::LiteralInteger, "1");
        assert_tag!(lexer, Tag::Plus);
        assert_token_lexeme!(lexer, Tag::LiteralInteger, "2");
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_number_float() {
        let input = "11.0f 1234.56789 22.0F 1.0l 1.0L 99e2";
        let mut lexer = Lexer::new(input);

        assert_token_lexeme!(lexer, Tag::LiteralFloat, "11.0f");
        assert_token_lexeme!(lexer, Tag::LiteralFloat, "1234.56789");
        assert_token_lexeme!(lexer, Tag::LiteralFloat, "22.0F");
        assert_token_lexeme!(lexer, Tag::LiteralFloat, "1.0l");
        assert_token_lexeme!(lexer, Tag::LiteralFloat, "1.0L");
        assert_token_lexeme!(lexer, Tag::LiteralFloat, "99e2");
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_keyword() {
        let input = "\
            auto \
            break \
            case char const continue \
            default do double \
            else enum extern \
            float false for \
            goto \
            if \
            int \
            long \
            register return \
            short signed sizeof static struct switch \
            true typedef \
            union unsigned \
            void volatile \
            while";

        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::Auto);
        assert_tag!(lexer, Tag::Break);
        assert_tag!(lexer, Tag::Case);
        assert_tag!(lexer, Tag::Char);
        assert_tag!(lexer, Tag::Const);
        assert_tag!(lexer, Tag::Continue);
        assert_tag!(lexer, Tag::Default);
        assert_tag!(lexer, Tag::Do);
        assert_tag!(lexer, Tag::Double);
        assert_tag!(lexer, Tag::Else);
        assert_tag!(lexer, Tag::Enum);
        assert_tag!(lexer, Tag::Extern);
        assert_tag!(lexer, Tag::Float);
        assert_tag!(lexer, Tag::False);
        assert_tag!(lexer, Tag::For);
        assert_tag!(lexer, Tag::Goto);
        assert_tag!(lexer, Tag::If);
        assert_tag!(lexer, Tag::Int);
        assert_tag!(lexer, Tag::Long);
        assert_tag!(lexer, Tag::Register);
        assert_tag!(lexer, Tag::Return);
        assert_tag!(lexer, Tag::Short);
        assert_tag!(lexer, Tag::Signed);
        assert_tag!(lexer, Tag::Sizeof);
        assert_tag!(lexer, Tag::Static);
        assert_tag!(lexer, Tag::Struct);
        assert_tag!(lexer, Tag::Switch);
        assert_tag!(lexer, Tag::True);
        assert_tag!(lexer, Tag::Typedef);
        assert_tag!(lexer, Tag::Union);
        assert_tag!(lexer, Tag::Unsigned);
        assert_tag!(lexer, Tag::Void);
        assert_tag!(lexer, Tag::Volatile);
        assert_tag!(lexer, Tag::While);
    }

    #[test]
    fn test_token_end_of_line() {
        let input = " \n\r\nHELLO\n";
        let mut lexer = Lexer::new(input);
        assert_tag!(lexer, Tag::EndOfLine);
        assert_tag!(lexer, Tag::EndOfLine);
        assert_token_lexeme!(lexer, Tag::Identifier, "HELLO");
        assert_tag!(lexer, Tag::EndOfLine);
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_line_comment() {
        let input = "//\n// 20 FOO\n// 30 BAR";
        let mut lexer = Lexer::new(input);
        assert_token_lexeme!(lexer, Tag::LineComment, "//");
        assert_tag!(lexer, Tag::EndOfLine);
        assert_token_lexeme!(lexer, Tag::LineComment, "// 20 FOO");
        assert_tag!(lexer, Tag::EndOfLine);
        assert_token_lexeme!(lexer, Tag::LineComment, "// 30 BAR");
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_block_comment() {
        let input = "/** comment **/\nFOO/*end";
        let mut lexer = Lexer::new(input);
        assert_token_lexeme!(lexer, Tag::BlockComment, "/** comment **/");
        assert_tag!(lexer, Tag::EndOfLine);
        assert_token_lexeme!(lexer, Tag::Identifier, "FOO");
        assert_token_lexeme!(lexer, Tag::BlockComment, "/*end");
        assert_tag!(lexer, Tag::EndOfFile);
    }

    #[test]
    fn test_token_character() {
        let input = "'a' '9' '_' '?'";
        let mut lexer = Lexer::new(input);
        assert_token_lexeme!(lexer, Tag::Character, "'a'");
        assert_token_lexeme!(lexer, Tag::Character, "'9'");
        assert_token_lexeme!(lexer, Tag::Character, "'_'");
        assert_token_lexeme!(lexer, Tag::Character, "'?'");
    }

    #[test]
    fn test_token_string() {
        let input = r###" "foo_bar" "1234567890abcdefghijklmnopqrstuvwxyz_!@#$%^&*()-=" "###;
        let mut lexer = Lexer::new(input);
        assert_token_lexeme!(lexer, Tag::String, r#""foo_bar""#);
        assert_token_lexeme!(
            lexer,
            Tag::String,
            r#""1234567890abcdefghijklmnopqrstuvwxyz_!@#$%^&*()-=""#
        );
    }
}
