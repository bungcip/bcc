TODO:
Preprocessor
- handle cpp process
- handle #include
- handle #define
- handle #undef
- handle #if, #else, #endif, #ifdef
- handle __FILE__, __LINE__
- handle #pragma
- handle #error #warning
- handle #line


Lexer:
- Someday parse UTF8
- support escape (backslash)
- [DONE] parse constant character from C
- [DONE] parse constant string from C
- [DONE] Loc & Span & Pos

Parser:
- parse complex type (enum, storage specifier, typedef, struct)
- parse pointer type
- parse const/volatile type
- parse initilizer expr ({..})
- split decl to spec & decl
- parse const/static
- parse expression
- parse decl
- parse address-of expr
- parse complex function decl/def
- [DONE] parse cast expr
- [DONE] parse float literal
- [DONE] parse char literal
- [DONE] fix expression associaty
- [DONE] parse typedef
- [DONE] parse empty stmt (;)
- [DONE] fix expression precedence
- [DONE] parse comma expr
- [DONE] parse simple function def
- [DONE] parse simple function decl
- [DONE] parse variable declaration
- [DONE] parse enum
- [DONE] parse struct
- [DONE] parse union
- [DONE] parse switch
- [DONE] parse case stmt
- [DONE] parse label stmt
- [DONE] parse default stmt
- [DONE] parse simple type
- [DONE] parse elvis ( ? : )
- [DONE] parse comparion expr (< > <= >= == !=)
- [DONE] parse bit and or xor
- [DONE] parse logical and or
- [DONE] parse unary
- [DONE] parse pointer/deref
- [DONE] parse dot/arrow access
- [DONE] parse pre/post increment/decrement
- [DONE] parse array access
- [DONE] parse sizeof
- [DONE] parse continue/break
- [DONE] parse goto
- [DONE] parse for
- [DONE] parse while
- [DONE] parse do-while
- [DONE] parse if 
- [DONE] parse function call
- [DONE] parse assigment
- [DONE] parse binary expression (+ * - /)
- [DONE] parse op precedence
- [DONE] parse statement
- [DONE] parse return 

Visitor
- [DONE] visitor trait
- generate code to visit AST

Generator
- produce simple asm code

Driver
- use clap to drive compiler

AST
- Simple Node

CST
- Simple Node

MILESTONE 1
- [DONE] produce tokenizer example
- [ONGOING] produce AST dumper example
- produce csmith test

MILESTONE 2
- produce binary program which run factorial