extern crate clap;
extern crate bcparser;

use std::process;
use std::fs::File;
use std::io::prelude::*;
use clap::Parser;
use bcparser::*;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// c source code file
    filename: String,
}


fn main() {
    let args = Cli::parse();
    run(&args.filename);
}

fn run(path: &str) {
    let mut file = match File::open(path){
        Ok(file) => file,
        Err(why) => {
            println!("Error: {}", why);
            process::exit(255);
        }
    };
    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("cannot fill contents");

    let result = bcparser::tokenize(&contents).unwrap();


    println!("Index  |     Loc     | Token");
    println!("----------------------------");

    for (i, token) in result.tokens.iter().enumerate() {
        print_token(&result.bookmark, i, token);
    }
}

fn print_token(bookmark: &Bookmark, index: usize, token: TokenRef){
    let loc = bookmark.decode_offset(token.start.clone());
    println!(" {:>5} |  ({:>3}, {:>3}) | {:45?}  ", index, loc.line, loc.column, token.tag );
}