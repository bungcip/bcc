extern crate clap;
extern crate bcparser;

use std::process;
use std::fs::File;
use std::io::prelude::*;
// use clap::{Parser, Arg};
use bcparser as bc;
use bc::*;
use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// c source code file
    #[arg(short, long)]
    input: String,
}

fn main() {
    let args = Args::parse();
    run(&args.input);
}

fn run(path: &str) {
    let mut file = match File::open(path){
        Ok(file) => file,
        Err(why) => {
            println!("Error: {}", why);
            process::exit(255);
        }
    };

    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("cannot fill contents");

    let result = bc::tokenize(&contents).unwrap();
    let result = bc::parse(result.tokens, result.bookmark).unwrap();

//    println!("TU: {:?}", result);
    dump(result);
}

fn dump(tu: bc::TranslationUnit){
    let mut visitor = AstDumper {
        indent_level: 0,
        after_segment: false,
    };

    visitor.visit_translation_unit(&tu);
}


struct AstDumper {
    indent_level: usize,
    after_segment: bool,
}

impl AstDumper {
    pub fn print(&mut self, text: &str){
        if self.after_segment {
            println!("{text}", text=text);
            self.after_segment = false;
        } else {
            println!("{:<width$}{text}", " ", width = self.indent_level * 2, text=text);
        }
    }
    
    pub fn segment(&mut self, text: &str){
        print!("{:<width$}:{text}", " ", width = self.indent_level * 2, text=text);
        self.after_segment = true;
    }

}

impl bc::Visitor for AstDumper {
    fn visit_translation_unit(&mut self, node: &TranslationUnit) {
        self.print("[Translation Unit]");

        self.indent_level += 1;
        for decl in &node.decls {
            self.visit_decl(decl);
        }
        self.indent_level -= 1;
    }

    fn visit_function_def(&mut self, node: &FunctionDef) {
        let name = node.name();
        let ty = node.ty();
        let output = format!("[FunctionDef] {} {}", ty, name);
        self.print(&output);

        // println!("count:{}", &node.1.len());

        self.indent_level += 1;
        for child in &node.1 {
            self.visit_stmt(&child)
        }
        self.indent_level -= 1;
    }

    fn visit_call(&mut self, node: &CallExpr){
//        println!("{:?}", node);
        let output = format!("[CallExpr]");
        self.print(&output);

        self.indent_level += 1;
        node.accept(self);
        self.indent_level -= 1;
    }

    fn visit_id(&mut self, node: &String){
        let output = format!("[NameExpr] {}", node);
        self.print(&output);
    }

    fn visit_string(&mut self, node: &StringLit){
        let output = format!("[StringLit] {}", node.0);
        self.print(&output);
    }

    fn visit_integer(&mut self, node: &IntegerLit){
        let output = format!("[IntegerLit] {}", node.0);
        self.print(&output);
    }

    fn visit_return_stmt(&mut self, node: &ReturnStmt){
        let output = format!("[ReturnStmt]");
        self.print(&output);
        self.indent_level += 1;
        self.segment("expr => ");
        node.accept(self);
        self.indent_level -= 1;
    }

    fn visit_decl_stmt(&mut self, node: &DeclStmt){
        let output = format!("[DeclStmt] {}", node.1);
        self.print(&output);
        self.indent_level += 1;
        self.print(&format!(":type => {}", node.0));
        
        if let Some(ref expr) = node.2 {
            self.segment("expr => ");
            self.indent_level += 1;
            expr.accept(self);            
            self.indent_level -= 1;
        }

        self.indent_level -= 1;
    }

    fn visit_binary(&mut self, node: &BinaryExpr){
        let output = format!("[BinaryExpr] op = {:?}", node.op);
        self.print(&output);
        self.indent_level += 1;

        self.segment("left  => ");
        node.left.accept(self);

        self.segment("right => ");
        node.right.accept(self);

        self.indent_level -= 1;
    }

    fn visit_cast(&mut self, node: &CastExpr){
        let output = format!("[CastExpr]");
        self.print(&output);

        self.indent_level += 1;
        self.print(&format!(":type => {}", node.0));
        self.segment("expr => ");
        node.1.accept(self);

        self.indent_level -= 1;
    }

}